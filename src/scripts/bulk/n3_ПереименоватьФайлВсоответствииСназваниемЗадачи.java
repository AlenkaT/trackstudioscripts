package scripts.bulk;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskBulkProcessor;

import com.trackstudio.secured.SecuredTaskBean;

import scripts.helpers.MessageGenerator;
import scripts.task_custom_field_value.FileNameCalculator;


public class n3_���������������������������������������������� implements TaskBulkProcessor {
	
	@Override
	public SecuredTaskBean execute(SecuredTaskBean task) throws GranException {
		// TODO Auto-generated method stub
		String name=task.getName();

		try
		{			
			ArrayList<File> files= FileNameCalculator.getFilesForTask(task);
			String text="";
			for (File file:files)
			{
				String fileName=file.getPath(); //������ ����
				Integer extIndex=file.getName().lastIndexOf(".");
				String shortName=file.getName().substring(0,extIndex);
				Files.move(Paths.get(fileName), Paths.get(fileName.replaceFirst(shortName,name)));		
				
			}
		}

		catch ( IOException e)
		{
			MessageGenerator.CreateCommentMsg(task, "�� ������� ������������� ����, ��� ��� ������ � ����� �����������", false);
		}
		return null;
		
		
	}

}
