package scripts.bulk;



import java.util.Calendar;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.app.session.SessionContext;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskBulkProcessor;
import com.trackstudio.kernel.manager.MessageManager;
import com.trackstudio.kernel.manager.SafeString;
import com.trackstudio.kernel.manager.TaskManager;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredUDFBean;
import com.trackstudio.secured.SecuredUserBean;

public class n2_�������������������������������������� implements TaskBulkProcessor {

	@Override
	public SecuredTaskBean execute(SecuredTaskBean task) throws GranException {
		// TODO Auto-generated method stub
		
		
		if (!task.getCategoryId().contentEquals("402881816f228b86016fa865c6d31eb1" )) return null;
		
		SessionContext sc= task.getSecure();
		
		String UDFReadUsersID="402881816ff5b7bf0170c9e521f44537"; //id ���� "��������" ��� ��������� ������
		
		//String readAllStatusID="402881816f228b86016fa86f8d8f1f86"; //������ ���������
		SecuredUDFBean UDFReadUsers = AdapterManager.getInstance().getSecuredFindAdapterManager().findUDFById(sc, UDFReadUsersID);
		//ArrayList<String> UDFReadUsersValue = (ArrayList<String>)task.getUdfValueByName(UDFReadUsers.getCaption());
		String UDFReadUsersValue =AdapterManager.getInstance().getSecuredUDFAdapterManager().getTaskUDFValue(sc, task.getId(), UDFReadUsers.getCaption());
		
		SecuredUserBean user= AdapterManager.getInstance().getSecuredFindAdapterManager().findUserById(sc, task.getSecure().getUserId());
		UDFReadUsersValue=UDFReadUsersValue+";"+user.getLogin();
		
		String UDFIDSubscribedUsers="402881816ff5b7bf0170ab1ea6323258"; //id ���� "���������� ���������" ��� ��������� ������
		SecuredUDFBean UDFSubscribedUsers = AdapterManager.getInstance().getSecuredFindAdapterManager().findUDFById(sc, UDFIDSubscribedUsers);
		String subscribedUserValue =AdapterManager.getInstance().getSecuredUDFAdapterManager().getTaskUDFValue(sc, task.getId(), UDFSubscribedUsers.getCaption());
		
		//AdapterManager.getInstance().getSecuredUDFAdapterManager().setTaskUDFValueSimple(sc, task.getId(), UDFReadUsers.getCaption(),UDFReadUsersValue);
		AdapterManager.getInstance().getSecuredUDFAdapterManager().setTaskUDFValueSimple(sc, task.getId(), UDFSubscribedUsers.getCaption(), subscribedUserValue.replaceAll("@"+user.getLogin(), ";" )); // �������� �������� ���� "���������� ���������"

		   //�������� , ��� �� �� ���, ���� ���������� ���������, ��������� ������
			//�������� ��������� ������ ����� ����� TaskManager
			//���������� ������ � �������� ���� �� �������� ����������� (����, ������������� ������� �� ����������� � ����)
			
		AdapterManager.getInstance().getSecuredMessageAdapterManager().createMessage(task.getSecure(), task.getId(), "402881816f228b86016fa86ec6721f61", "", null, task.getHandlerUserId(), null, null,
    			task.getPriorityId(), task.getDeadline(), task.getBudget(), false, Calendar.getInstance());
		
		
			String endStatusID="402881816ff5b7bf0170c8642b4040d5"; //������ ���������
			
	    	if (task.getStatusId()==endStatusID)
	    	{
	    		return null;
	    	}
			
	    	
	
			Boolean readAll=true;
			
			    String[] userNames = subscribedUserValue.replaceAll("@", "").split(";"); //���� �. ��������� //userNames[0] - ��������� ���������� �������� �����	
				for (String userName : userNames) 
				{
					// ��������� ������ ������ �����������
					if (!UDFReadUsersValue.contains(userName))
				    {	
						//appendedDescription= System.lineSeparator()+"������ ��������� ������, ���� ������������ "+ "@"+userName+" �� �������� ��� ������";
						readAll=false;
						break;
				    }
					
					
					
				}
				
			
		    	if ( readAll)
		    	{
		    		TaskManager.getTask().updateTaskStatus(task.getId(), "402881816f228b86016fa86f8d8f1f86"); //������������� ������ ���������
		    	
		    		
		    	/*	String messageId=MessageManager.getMessage().createMessage(task.getSecure().getUserId(), task.getId(), "402881816ff5b7bf0170c863d8a140a7", 
		    				SafeString.createSafeString(""), null, null, null, null, task.getPriorityId(), task.getDeadline(), task.getBudget(), Calendar.getInstance());*/
		    		String messageID = AdapterManager.getInstance().getSecuredMessageAdapterManager().createMessage(task.getSecure(), task.getId(), "402881816ff5b7bf0170c863d8a140a7", "", null, null, null, null,
		        			task.getPriorityId(), task.getDeadline(), task.getBudget(), false);
		    		if  (messageID!=null)
		    		{
		    			TaskManager.getTask().updateTaskStatus(task.getId(), endStatusID); //������������� ������ ���������
		    		}
		        			
		    	}

		    
		
    	return null;
		
	}

}
