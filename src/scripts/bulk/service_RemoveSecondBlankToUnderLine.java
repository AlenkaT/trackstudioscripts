package scripts.bulk;

import java.nio.file.Files;
import java.nio.file.Path;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskBulkProcessor;
import com.trackstudio.secured.SecuredTaskBean;

import scripts.helpers.MessageGenerator;

public class service_RemoveSecondBlankToUnderLine implements TaskBulkProcessor {

	
	@Override
	public SecuredTaskBean execute(SecuredTaskBean task) throws GranException {
		// TODO Auto-generated method stub
		String name=task.getName();
		String newName= name.replaceFirst(" ", "_").replaceFirst(" ", "_").replaceFirst("_", " ");
		AdapterManager.getInstance().getSecuredTaskAdapterManager().updateTask(task.getSecure(), task.getId(), task.getShortname(), newName,
				task.getDescription(), task.getBudget(), task.getDeadline(), task.getPriorityId(), task.getParentId(), task.getHandlerUserId(), task.getHandlerGroupId(), false);
		
	return task;
	}
}
