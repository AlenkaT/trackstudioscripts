package scripts.bulk;

import java.nio.file.Files;
import java.nio.file.Path;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskBulkProcessor;
import com.trackstudio.secured.SecuredTaskBean;

import scripts.helpers.MessageGenerator;
import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.ValueContainer;

public class service_SetProjectCode implements TaskBulkProcessor {

	
	@Override
	public SecuredTaskBean execute(SecuredTaskBean task) throws GranException {
		// TODO Auto-generated method stub
		UDFValueAccessor accessor=new UDFValueAccessor();
		String name=task.getName();

		int beginIndex= name.indexOf("_");
		if (beginIndex<0)
		{
		 return task;
		}
		
		int endIndex= name.indexOf(" ", beginIndex);
		String code="";
		if (endIndex<0)
		{
			code= name.substring(beginIndex+1);
		}
		else
		{
			code= name.substring(beginIndex+1, endIndex);
		}
		
		accessor.SetUDFValue(UDF_IDs.ShortSectionCode, task, code, true);
	return task;
	}
}
