package scripts.bulk;

import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskBulkProcessor;
import com.trackstudio.secured.SecuredTaskBean;


import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;


public class service_ResetErrorMessageField implements TaskBulkProcessor {

	
	@Override
	public SecuredTaskBean execute(SecuredTaskBean task) throws GranException {
		// TODO Auto-generated method stub
		UDFValueAccessor accessor=new UDFValueAccessor();
		
		accessor.SetUDFValue(UDF_IDs.ErrorMessage, task,"", true);		
	return task;
	}
}
