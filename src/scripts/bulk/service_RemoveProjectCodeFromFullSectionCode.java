package scripts.bulk;

import java.nio.file.Files;
import java.nio.file.Path;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskBulkProcessor;
import com.trackstudio.secured.SecuredTaskBean;

import scripts.helpers.MessageGenerator;
import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.ValueContainer;
import scripts.task_custom_field_value.SectionCodeCalculator;

public class service_RemoveProjectCodeFromFullSectionCode implements TaskBulkProcessor {

	
	
	@Override
	public SecuredTaskBean execute(SecuredTaskBean task) throws GranException {
		UDFValueAccessor accessor=new UDFValueAccessor();
		accessor.SetErrorUDFValue(task, "", true);
		ValueContainer vc = accessor.GetUDFValue(String.class, UDF_IDs.ShortSectionCode, task, false); 
		
		if (!vc.isExist())
		{		
			return task;
		}
		
		String code= (String)vc.getValue();
		String prCode=SectionCodeCalculator.GetProjectCode(task);
		String newValue=code.replaceAll(prCode, " ").replaceAll("-", "").replaceAll("�", "").trim();
				
		if (code.contains(prCode))
		{
			accessor.SetUDFValue(UDF_IDs.ShortSectionCode, task, newValue, true);
		}
		return task;
	}
}
