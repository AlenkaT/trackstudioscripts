package scripts.bulk;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskBulkProcessor;
import com.trackstudio.kernel.manager.SafeString;
import com.trackstudio.kernel.manager.TaskManager;
import com.trackstudio.secured.SecuredMessageTriggerBean;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredUDFBean;
import com.trackstudio.secured.SecuredUserUDFBean;

import scripts.helpers.MessageGenerator;
import scripts.task_custom_field_value.FileNameCalculator;

public class n4_������������������ implements TaskBulkProcessor {
	
	@Override
	public SecuredTaskBean execute(SecuredTaskBean task) throws GranException {
		String name=task.getName();
		String[] lines=name.split("_");
		String[] nums=lines[0].split("\\.");
		try
		{
			
			int number=Integer.parseInt(nums[0]);
			
			ArrayList<File> files= FileNameCalculator.getFilesForTask(task);
			String text="";
			for (File file:files)
			{
				String fileName=file.getPath();
				String newName=file.getName().replaceFirst(nums[0], String.format("%02d",number-1));
		//		Files.move(Paths.get(fileName), Paths.get(fileName.replaceFirst(file.getName(), newName)));
			//	text=text+"���� ��� ������������: <br>"+file.getName()+" -> "+newName+"<br>";
			}
			//if (!text.isEmpty()) MessageGenerator.CreateCommentMsg(task, text , false);
			AdapterManager.getInstance().getSecuredTaskAdapterManager().updateTask(task.getSecure(), task.getId(), task.getShortname(), 
					name.replaceFirst(String.valueOf(number),String.valueOf(number-1)),
					task.getDescription(), task.getBudget(), task.getDeadline(), task.getPriorityId(), task.getParentId(), task.getHandlerUserId(), task.getHandlerGroupId(), false);		
		}
		catch (NumberFormatException e)
		{
			
		}
		/*catch ( IOException e)
		{
			MessageGenerator.CreateCommentMsg(task, "�� ������� �������� ����� ������, ������ � ����� �����������", false);
			
		}*/
		return null;
				
	}

}
