package scripts.bulk;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.exception.UserException;
import com.trackstudio.external.TaskBulkProcessor;

import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredUDFBean;
import com.trackstudio.secured.SecuredUserUDFBean;

import scripts.helpers.RequestGenerator;
import scripts.helpers.TaskCreator;
import scripts.helpers.TaskFieldValuesChecker;
import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.ValueContainer;

public class n5_�������������������������������������������������� implements TaskBulkProcessor {
	
	@Override
	public SecuredTaskBean execute(SecuredTaskBean task) throws GranException {
		// TODO Auto-generated method stub
		
		UDFValueAccessor udfAccessor= new UDFValueAccessor();
			  
		//���� � �����
		ValueContainer vc=udfAccessor.GetAvailableDirPath(task, false);
		
		if (vc.isExist())
		{
				  String fullDirName=((String)vc.getValue());
				  vc=udfAccessor.GetUDFValue("String", UDF_IDs.SelfDir, task, false);
				  if (vc.isExist())
				  {
				 	try {
					  	File selfDir=	new File(TaskFieldValuesChecker.getServerDirPath(fullDirName));
					  	int ind=fullDirName.lastIndexOf((String)vc.getValue());
					  	if (selfDir.isDirectory() && ind>0)
					  	{
					  		
					  		if (selfDir.renameTo(new File(TaskFieldValuesChecker.getServerDirPath(fullDirName.substring(0,ind)+task.getName()))))
					  		{
					  			udfAccessor.SetUDFValueByAdmin(UDF_IDs.SelfDir, task, task.getName(), true);  //����������� ��� ����������� �� ���� �������
					  		}
					  		else
					  		{
					  			udfAccessor.SetErrorUDFValue(task, fullDirName.substring(0,ind)+task.getName(),true);
					  		}
					  	}
				  	}
				 	catch (Exception ex) {
					// TODO Auto-generated catch block
					// ���������� ����������
					 TaskCreator.CreateTaskForAdminAboutException("execute", this.getClass().getName(), "�� ������� ��������� ������ ���������", task.getNumber(), ex);
				 	}
				  }
		}
		return null;
	}

}
