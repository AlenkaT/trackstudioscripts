package scripts.bulk;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskBulkProcessor;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredUDFBean;
import com.trackstudio.secured.SecuredUserUDFBean;

import scripts.helpers.SimpleLogger;
import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;

public class n5_������������������������������������������ implements TaskBulkProcessor {

	private UDFValueAccessor udfAccessor= new  UDFValueAccessor();
	private SimpleLogger logger= new  SimpleLogger();
	
	@Override
	public SecuredTaskBean execute(SecuredTaskBean task) throws GranException {
		// TODO Auto-generated method stub
			
			SecuredUDFBean udfBean = AdapterManager.getInstance().getSecuredFindAdapterManager().findUDFById(task.getSecure(), UDF_IDs.TemplatePath);
			HashMap<String, String> list=udfBean.getUL();
			
			list.forEach((k, v) -> 
				
					{
						try {
							if (k.contentEquals(UDF_IDs.ParentTemplateDirPathKey))
							{
								udfAccessor.SetUDFValue(UDF_IDs.SelfDir, task , "", true);
								udfAccessor.SetUDFValue(UDF_IDs.TemplatePath, task , v, true);
							}
						} catch (GranException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				);
			
			return task;
		}

}
