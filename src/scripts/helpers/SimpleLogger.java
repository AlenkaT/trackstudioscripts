package scripts.helpers;

import java.io.FileWriter;
import java.io.IOException;

import com.trackstudio.exception.GranException;

public class SimpleLogger {
	private String _requestDirName;
	
	public void Write(String message, String fileName, boolean overwrite, String taskNumber) throws GranException {
		
		String fullFilePath=_requestDirName+"\\"+fileName+".txt";
		try(FileWriter fos=new FileWriter( fullFilePath, overwrite))
		{
			fos.write(message);//   
			fos.close();
		}
		catch(IOException ex)
		{			
			TaskCreator.CreateTaskForAdmin("���������� IOException ��� ������� ������ � ���� "+fullFilePath, taskNumber);
		}
	}
	
		public SimpleLogger(String requestDirName) {
			_requestDirName=requestDirName;
		}
		public SimpleLogger() {
			_requestDirName="D:\\NetworkDrives\\Obmen\\��������\\Tests\\Logs\\";
		}
	
}
