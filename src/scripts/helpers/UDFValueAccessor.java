package scripts.helpers;


import java.util.Calendar;
import java.util.List;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.app.session.SessionContext;
import com.trackstudio.app.session.SessionManager;
import com.trackstudio.secured.SecuredMessageTriggerBean;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredTaskTriggerBean;
import com.trackstudio.secured.SecuredUDFBean;
import com.trackstudio.secured.SecuredUserBean;
import com.trackstudio.secured.SecuredUserUDFBean;
import java.util.ArrayList;

import scripts.task_custom_field_value.RemouteDirPathCalculator;

import com.trackstudio.exception.GranException;
import com.trackstudio.kernel.cache.UserRelatedManager;

public class UDFValueAccessor{
	
	private Boolean IsClassNamePossible(String className, Object value)
	{
		switch (className) {
			case "String": return value instanceof String;
			case "Integer": return 	value instanceof Integer;
			case "Double": return 	value instanceof Double;
			case "Calendar": return 	value instanceof Calendar;
			case "Pair": return 	value instanceof com.trackstudio.tools.Pair;
			case "List<Pair>": return 	value instanceof List<?> && !((ArrayList<?>)value).isEmpty() && ((ArrayList<?>)value).get(0) instanceof com.trackstudio.tools.Pair;
			case "ArrayList<SecuredTaskBean>": return 	value instanceof ArrayList<?> && !((ArrayList<?>)value).isEmpty() && ((ArrayList<?>)value).get(0) instanceof SecuredTaskBean;
			case "ArrayList<SecuredUserBean>":  return 	value instanceof ArrayList<?> && !((ArrayList<?>)value).isEmpty() && ((ArrayList<?>)value).get(0) instanceof SecuredUserBean;
			case "Link": return 	value instanceof com.trackstudio.containers.Link;
		}
		return false;
	}
	 
	public UDFValueAccessor()
	{
		
	}
	
	public ValueContainer GetUDFValue(String id, SecuredMessageTriggerBean arg0, boolean checkParent) throws GranException 
	{
		//������ ��� ���������, ��� udfBean �������� ��� ������ (����� ������� ��� ����� ������� , ��� udfBean �������� ��� ��������������!)
		String msgSuffix=" id="+id;
		SecuredUDFBean udfBean = AdapterManager.getInstance().getSecuredFindAdapterManager().findUDFById(arg0.getSecure(), id);
		
		if (udfBean==null)  //����� ���� ��������� ����������� � TS ��� ��� ���� �������
		{
			return new ValueContainer(StringConstants.AccessUDFError+msgSuffix, "NoName");
		}
		String value=arg0.getUdfValue(udfBean.getCaption());
		
		if (value==null ) // �������� �� ������
		{
			if (checkParent)
			{
				return GetUDFValue("String", id, arg0.getTask().getParent(),true);
			}
			else
			{
				//� ������������ ������ ����� �������� ������ ������, ���� ���� ������ ��� ������
				return new ValueContainer((Object)"", udfBean.getCaption()); //�������� ������ ����������� � �� ������������ ��� ��������� ������	
			}
		}
		return new ValueContainer((Object)value, udfBean.getCaption());
	}
		
	public ValueContainer GetUDFValue(String className, String id, SecuredTaskBean arg0, boolean checkParent) throws GranException 
	{
		//String fileName="GetUDFValue";
		String msgSuffix=" id="+id;
		//SimpleLogger.Write("arg0="+arg0, fileName, true);
		SecuredUDFBean udfBean = AdapterManager.getInstance().getSecuredFindAdapterManager().findUDFById(arg0.getSecure(), id);
		if (udfBean==null) //����� ���� ��������� ����������� � TS ��� � ������������ ��� ������� � ����
		{
			return new ValueContainer(StringConstants.AccessUDFError+msgSuffix, "NoName");
		}
		
		//SimpleLogger.Write(udfBean, fileName, true);
		Object value=arg0.getUdfValueByName(udfBean.getCaption());	
					//������ ������ � ���� ����� �������� ��� null!!! :-0
		if (value==null ) //�������� �� ���� ������. ������ ����� ���� ���� ������� ����� �������� ������
		{
			if (checkParent)
			{
				return GetUDFValue(className, id, arg0.getParent(),true);
			}
			else
			{
				//���� ������ �� ���������� ����� ����� ���������� �����
				if (className.contentEquals("String"))
				{
					return new ValueContainer((Object)"", udfBean.getCaption());
				}
				else
				{
					//return new ValueContainer(StringConstants.ValueUDFError, udfBean.getCaption());
					ValueContainer vc=ValueContainer.getEmpty();
					//return new ValueContainer(new Object(), udfBean.getCaption()); // ������ ������ ������ Object- ���� ������� ������ ��� ��� ���������� � ������ ���� ������� ����������
					return vc;
				}
			}
		}
		
		
		if (!IsClassNamePossible(className, value))
		{
			return new ValueContainer(StringConstants.CastValueUDFError+msgSuffix, udfBean.getCaption());
		}
		return new ValueContainer(value, udfBean.getCaption());
	}	
	
	public ValueContainer GetUDFValueByAdmin(String id, SecuredTaskBean arg0, boolean checkParent) throws GranException 
	{
		//String fileName="GetUDFValue";
		String msgSuffix=" id="+id;
		//SimpleLogger.Write("arg0="+arg0, fileName, true);
		SessionContext sc= SessionManager.getInstance().getSessionContext(SessionManager.getInstance().create(UserRelatedManager.getInstance().find(StringConstants.AdminId)));
		SecuredUDFBean udfBean = AdapterManager.getInstance().getSecuredFindAdapterManager().findUDFById(sc, id);
		if (udfBean==null) //����� ���� ��������� ����������� � TS
		{
			return new ValueContainer(StringConstants.AccessUDFError+msgSuffix, "None");
		}
		
		//SimpleLogger.Write(udfBean, fileName, true);
		String value=AdapterManager.getInstance().getSecuredUDFAdapterManager().getTaskUDFValue(sc, arg0.getId(), udfBean.getCaption());
		
					//������ ������ � ���� ����� �������� ��� null!!! :-0
		if (value==null ) //�������� �� ���� ������. ������ ����� ���� ���� ������� ����� �������� ������
		{
			if (checkParent)
			{
				return GetUDFValueByAdmin( id, arg0.getParent(),true);
			}
			else
			{
				return new ValueContainer((Object)"", udfBean.getCaption());
			}
		}
		return new ValueContainer((Object)value, udfBean.getCaption());
		
	}
	
	public ValueContainer GetUDFValue(String id, SecuredTaskTriggerBean arg0) throws GranException 
	{//String globalDirValue=(SecuredTaskTriggerBean)arg0).getUdfValue(udfBean.getCaption());
		// ��� Trigger �� ����������������� ����� �� ���������
		//String fileName="GetUDFValue";
		String msgSuffix=" id="+id;
		//SimpleLogger.Write("arg0="+arg0, fileName, true);
		SecuredUDFBean udfBean = AdapterManager.getInstance().getSecuredFindAdapterManager().findUDFById(arg0.getSecure(), id);
		if (udfBean==null) //����� ���� ��������� ����������� � TS
		{
			return new ValueContainer(StringConstants.AccessUDFError+msgSuffix, "None");
		}
		
		//SimpleLogger.Write(udfBean, fileName, true);
		Object value=arg0.getUdfValue(udfBean.getCaption());	
		//������ ������ � ���� ����� �������� ��� null!!! :-0
		if (value==null ) //�������� �� ���� ������. 
		{
			return new ValueContainer((Object)"", udfBean.getCaption());
		}	
		return new ValueContainer(value, udfBean.getCaption());
	}
	

	public void SetUDFValue(String id, SecuredTaskBean arg0, String value, boolean isBulk) throws GranException 
	{
		if (isBulk)
		{
			AdapterManager.getInstance().getSecuredUDFAdapterManager().setTaskUDFValueSimple(arg0.getSecure(), arg0.getId(),AdapterManager.getInstance().getSecuredFindAdapterManager().findUDFById(arg0.getSecure(), id).getCaption(), value);
		}
		else
		{
			AdapterManager.getInstance().getSecuredUDFAdapterManager().setTaskUdfValue(arg0.getSecure(), id, arg0.getId(), value);
		}
	}
	
	public void SetUDFValueByAdmin(String id, SecuredTaskBean arg0, String value, boolean isBulk) throws GranException 
	{
		SessionContext sc= SessionManager.getInstance().getSessionContext(SessionManager.getInstance().create(UserRelatedManager.getInstance().find(StringConstants.AdminId)));
		
		if (isBulk)
		{
			AdapterManager.getInstance().getSecuredUDFAdapterManager().setTaskUDFValueSimple(sc, arg0.getId(),AdapterManager.getInstance().getSecuredFindAdapterManager().findUDFById(sc, id).getCaption(), value);
		}
		else
		{
			AdapterManager.getInstance().getSecuredUDFAdapterManager().setTaskUdfValue(	sc, id, arg0.getId(), value);
		}
	}	
	
	public  void SetErrorUDFValue(SecuredTaskBean task, String value,boolean isBulk) throws GranException 
	{
		//��� ������ ����, ���������� ������ �������� � ������ �����������!

		SetUDFValueByAdmin(UDF_IDs.ErrorMessage, task, value, isBulk);
	}
	
	
	public  void AddErrorUDFValue(SecuredTaskBean task, String value,boolean isBulk) throws GranException 
	{
		//��� ������ ����, ���������� ������ �������� � ������ �����������!

		ValueContainer vc= GetUDFValueByAdmin(UDF_IDs.ErrorMessage, task, false);
		if (!vc.isEmpty()) value=vc.getValue()+"<br>"+value;
		SetUDFValueByAdmin(UDF_IDs.ErrorMessage, task, value, isBulk);
	}	
	
	
	public ValueContainer GetUserUDFValue(String id, SessionContext sc) throws GranException 
	{
		String msgSuffix=" id="+id;
		
		SecuredUserUDFBean udfUserBean = AdapterManager.getInstance().getSecuredFindAdapterManager().findUserUDFById(sc, id);

		if (udfUserBean==null)  //����� ���� ��������� ����������� � TS
		{
			return new ValueContainer(StringConstants.AccessUDFError+msgSuffix, "None");
		}
		
		String value = AdapterManager.getInstance().getSecuredUDFAdapterManager().getUserUDFValue(sc, sc.getUserId(), udfUserBean.getCaption());

		if (value==null )
		{
			//���� ������ �� ���������� ����� ����� ���������� �����
			return new ValueContainer(StringConstants.ValueUDFError+msgSuffix, udfUserBean.getCaption());
		}
		return new ValueContainer((Object)value, udfUserBean.getCaption());
	}
	
	
	
	
	public ValueContainer GetAvailableDirPath(SecuredTaskBean task, boolean checkParent) throws GranException 
	{
		
		//ValueContainer vc= UDFValueAccessor.GetUDFValue(String.class, UDF_IDs.FullPathDir, task, false); //��� ���� �������� �������� null, ������ ��� ��� ���������������
		String udfName="FullDirPath - ������ ���� � ����� �� �������";
		String path=RemouteDirPathCalculator.GetFullDirPath(task); //���� �� ������ ������ �������� null
		
		
		if (path!=null && !path.isEmpty()) return  new ValueContainer((Object)path, udfName);
		
		ValueContainer vc;
		if (task instanceof SecuredTaskTriggerBean) {
			
			vc = GetUDFValue(UDF_IDs.SelfDir, (SecuredTaskTriggerBean)task);
		}
		else
		{
			vc = GetUDFValue("String", UDF_IDs.SelfDir, task, false);
		}	
		
		if (!vc.isExist() || ((String)vc.getValue()).isEmpty())  
		{
			if (checkParent)
			{
				if (task.getNumber()==null) {return GetAvailableDirPath(task, true);}
				else
				{
					return GetAvailableDirPath(task.getParent(), true);
				}
			}
		}
		return vc;
		
		
		
		
		
		//return new ValueContainer(StringConstants.ValueUDFError+": ���� � ����� ��� ������ �� ������", udfName);
		//UDFValueAccessor.SetErrorUDFValue(task, "RemouteDirPathCalculator:"+ UDF_IDs.SelfDir+":"+task.getNumber());
		//return UDFValueAccessor.GetUDFValue(String.class, UDF_IDs.SelfDir, task, false);
	}
}	


