package scripts.helpers;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.secured.SecuredMessageTriggerBean;
import com.trackstudio.secured.SecuredTaskBean;

import scripts.task_custom_field_value.FileNameCalculator;

public class RequestGenerator {
	public RequestGenerator () {}
	
	public void CreateOpenExplorerRequest(SecuredMessageTriggerBean arg0) throws GranException
	{
		UDFValueAccessor udfAccessor= new UDFValueAccessor();
	  	// ��������� ������ �� �������� � ����������			
			// ������� ��� ������������
		ValueContainer vcUser=udfAccessor.GetUserUDFValue(UserUDF_IDs.NetWorkUserName, arg0.getSecure());
		if (vcUser.isExist())			
		{
			ValueContainer vc=udfAccessor.GetAvailableDirPath(arg0.getTask(), false);
			//ValueContainer vc=udfAccessor.GetUDFValue(String.class, UDF_IDs.SelfDir, arg0.getTask(), false);
			if (vc.isExist() && !((String)vc.getValue()).isEmpty())
			{
				String requestDirName="D:\\NetworkDrives\\Obmen\\��������\\Tests\\TrackStudio\\PullRequest\\Show\\"+vcUser.getValue()+"\\";
				
				//���� ����� ����� �� ���������� - �������
					File dir = new File(requestDirName);
					if (!dir.exists() && !dir.mkdirs()) // 
					{
						// �� ������� ������� ������� ��� ���������� �������
						TaskCreator.CreateTaskForAdmin("��� ���������� �������� "+" ������������� @"+arg0.getSubmitter().getLogin()+"�� ������� ������� ����������� ��� ���������� ������� �� �������� "+ requestDirName, arg0.getTask().getTaskNumber());
					}
					else
					{
						File[] listFiles= dir.listFiles();
						for(File file:listFiles)
						{
							file.delete();
						}
						//�������� ������������� �������� �� ���������, ��� ��� ���� �� �������� ��� ��������� � ���� ��������
						String gD =(String)vc.getValue();
						
						SimpleLogger logger= new SimpleLogger(requestDirName);
					//���������� ������ � �������� ���� �� �������� ����������� (����, ������������� ������� �� ����������� � ����)
						logger.Write(gD, arg0.getTask().getNumber(), false, arg0.getTask().getNumber());						
					}
			}
			else
			{
				// ������ ��� ��������� �������� �������� ��������
				DescriptionAccessor.AppendMessageDescription(arg0, "�������� �������� �� ��������. "+vc.toString());
			}
		}
		else
		{
			// ������ ��� ��������� �������� �������� WindowAccountName
			DescriptionAccessor.AppendMessageDescription(arg0, "�������� �������� �� ��������. "+vcUser.toString()+" ��� ���������� ������ ���������� �������� Windows Account Name (��� ������� ������ ������������ Window ������ ���������� ����������) � ����� �������");
		}
	}
	
	public void CreateOpenExplorerRequest(SecuredTaskBean task) throws GranException
	{
		UDFValueAccessor udfAccessor= new UDFValueAccessor();
		//udfAccessor.SetErrorUDFValue(task,"", true);
	  	// ��������� ������ �� �������� � ����������			
			// ������� ��� ������������
		ValueContainer vcUser=udfAccessor.GetUserUDFValue(UserUDF_IDs.NetWorkUserName, task.getSecure());
		//udfAccessor.SetErrorUDFValue(task, "�������� ���������� �������", true);
		if (vcUser.isExist() && !((String)vcUser.getValue()).isEmpty())			
		{
			//ValueContainer vc=udfAccessor.GetUDFValue(String.class, UDF_IDs.SelfDir, task, false);
			ValueContainer vc=udfAccessor.GetAvailableDirPath(task, true);
			//udfAccessor.SetErrorUDFValue(task, vc.toString(), true);
			if (vc.isExist())
			{
				String requestDirName="D:\\NetworkDrives\\Obmen\\��������\\Tests\\TrackStudio\\PullRequest\\Show\\"+vcUser.getValue();
				String requestDirName2="D:\\NetworkDrives\\Obmen\\��������\\Tests\\TrackStudio\\RequestPull\\"+vcUser.getValue()+"\\Explorer";
				//���� ����� ����� �� ���������� - �������
					File dir2 = new File(requestDirName2);
					if (!dir2.exists() && !dir2.mkdirs()) // 
					{
						// �� ������� ������� ������� ��� ���������� �������
						TaskCreator.CreateTaskForAdmin("��� ���������� ������� ������������� @"+task.getSecure().getUser().getLogin()+" �� ������� ������� ����������� ��� ���������� ������� �� �������� "+ requestDirName2, task.getTaskNumber());
						
					}
					File dir = new File(requestDirName);
					if (!dir.exists() && !dir.mkdirs()) // 
					{
						// �� ������� ������� ������� ��� ���������� �������
						TaskCreator.CreateTaskForAdmin("��� ���������� ������� ������������� @"+task.getSecure().getUser().getLogin()+" �� ������� ������� ����������� ��� ���������� ������� �� �������� "+ requestDirName, task.getTaskNumber());
						return;
					}
						
						SimpleLogger logger= new SimpleLogger(requestDirName);
						SimpleLogger logger2= new SimpleLogger(requestDirName2);
						for(File file:dir.listFiles())
						{
							file.delete();	
						}
						for(File file: dir2.listFiles())
						{
							file.delete();	
						}
						//�������� ������������� �������� �� ���������, ��� ��� ���� �� �������� ��� ��������� � ���� ��������
						String gD =(String)vc.getValue();
						//udfAccessor.SetErrorUDFValue(task,gD, true);
						//Path path= Paths.get(gD.replaceAll("Q:","D:\\\\NetworkDrives\\\\Object").replaceAll("T:","D:\\\\NetworkDrives\\\\Mail").replaceAll("S:", "D:\\\\NetworkDrives\\\\Obmen").replaceAll("R:", "D:\\\\NetworkDrives\\\\Arxiv"));
						//TaskFieldValuesChecker.CheckFullPathDir(task, "������� ������� ��������� �� ������");
						//SimpleLogger logger= new SimpleLogger(requestDirName);
					//���������� ������ � �������� ���� �� �������� ����������� (����, ������������� ������� �� ����������� � ����)
						logger.Write(gD, task.getNumber(), false, task.getNumber());
						logger2.Write(gD, task.getNumber(), false, task.getNumber());
					
			}
			else
			{
				// ������ ��� ��������� �������� �������� ��������			
				TaskCreator.CreateTaskForAdmin("��� ���������� ������� "+" ������������� @"+task.getSecure().getUser().getLogin()+" �������� �������� �� ��������. "+vc.toString(), task.getTaskNumber());
				
			}
		}
		else
		{
			// ������ ��� ��������� �������� �������� WindowAccountName
			
			TaskCreator.CreateTaskForAdmin("��� ���������� ������� "+" ������������� @"+task.getSecure().getUser().getLogin()+" �������� �������� �� ��������.  ��� ���������� ������ ���������� �������� Windows Account Name (��� ������� ������ ������������ Window ������ ���������� ����������) � ����� �������", task.getTaskNumber());
			
		}
	}
	
	
	
	
	public void CreateOpenFileRequest(SecuredTaskBean task) throws GranException
	{
		UDFValueAccessor udfAccessor= new UDFValueAccessor();
		//udfAccessor.SetErrorUDFValue(task,"", true);
	  	// ��������� ������ �� �������� � ����������			
			// ������� ��� ������������
		ValueContainer vcUser=udfAccessor.GetUserUDFValue(UserUDF_IDs.NetWorkUserName, task.getSecure());
		//udfAccessor.SetErrorUDFValue(task, "�������� ���������� �������", true);
		if (vcUser.isExist() && !((String)vcUser.getValue()).isEmpty())			
		{
			//	String requestDirName="D:\\NetworkDrives\\Obmen\\��������\\Tests\\TrackStudio\\PullRequest\\Show\\"+vcUser.getValue()+"\\";
				String requestDirName2="D:\\NetworkDrives\\Obmen\\��������\\Tests\\TrackStudio\\RequestPull\\"+vcUser.getValue()+"\\Open\\";
				//���� ����� ����� �� ���������� - �������
					File dir = new File(requestDirName2);
					if (!dir.exists() && !dir.mkdirs()) // 
					{
						// �� ������� ������� ������� ��� ���������� �������
						TaskCreator.CreateTaskForAdmin("��� ���������� ������� ������������� @"+task.getSecure().getUser().getLogin()+" �� ������� ������� ����������� ��� ���������� ������� �� �������� "+ requestDirName2, task.getTaskNumber());
					}
					else
					{
						File[] listFiles= dir.listFiles();
						SimpleLogger logger2= new SimpleLogger(requestDirName2);
						for(File file:listFiles)
						{
							file.delete();	
						}
						ArrayList<File> taskFileList= FileNameCalculator.getFilesForTask(task);
						//�������� ������������� �������� �� ���������, ��� ��� ���� �� �������� ��� ��������� � ���� ��������
						//���������� ������ � �������� ���� �� �������� ����������� (����, ������������� ������� �� ����������� � ����)
						for (File file:taskFileList)
						{
							logger2.Write(TaskFieldValuesChecker.getRemoteDirPath(file.getPath()), task.getNumber()+taskFileList.indexOf(file), false, task.getNumber());
						}
					}
			
		}
		else
		{
			// ������ ��� ��������� �������� �������� WindowAccountName
			
			TaskCreator.CreateTaskForAdmin("��� ���������� ������� "+" ������������� @"+task.getSecure().getUser().getLogin()+" �������� �������� �� ��������.  ��� ���������� ������ ���������� �������� Windows Account Name (��� ������� ������ ������������ Window ������ ���������� ����������) � ����� �������", task.getTaskNumber());
			
		}
	}
}
