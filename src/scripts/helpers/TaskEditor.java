package scripts.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.trackstudio.exception.GranException;
import com.trackstudio.kernel.manager.SafeString;
import com.trackstudio.kernel.manager.TaskManager;
import com.trackstudio.secured.SecuredTaskBean;

public class TaskEditor {

	private static String getSectionNumber(SecuredTaskBean task)
	{
		String name=task.getName();
		String[] lines=name.split("_");
		return lines[0];
	}
	
	public static void ChangeTaskNumbers(SecuredTaskBean task, int delta, boolean newTask) throws GranException
	{
	//� ������ ���������� ������ � �������� ������, ����� ������ ������ ��������� ������ ������->!!!!!�������� ������ ��� ������� ��� �������� (��� �����)
	 // !!!!!!!!!!!!!!!     �� ��������, ���� ��� ����� ����� ���� 2.1.5
	 String number=getSectionNumber(task); //������ ����� ������� ������
	 int index=number.lastIndexOf("."); //������ ��������� �����
	 String prefix="";
	 if (index>=0) prefix=number.substring(0, index+1); // endIndex+1 - 1�� �� ���������� ������
	 String shortNumber=number.substring(index+1);
		
	 Map<SecuredTaskBean,String> dictionary = new HashMap<SecuredTaskBean,String>();
	 //ArrayList<SecuredTaskBean> list= new ArrayList<SecuredTaskBean>();
	 
	 // ���� ������ ������ ���, �� ����� ����� �� �������
	 for (SecuredTaskBean t: task.getParent().getChildren())
	 {
		 if (t.getNumber()==task.getNumber()) continue; //���������� ������ ��� ��������� ������
		 if (t.getName().startsWith(prefix) )
		 {
			 String  t_number=getSectionNumber(t).substring(index+1); // ��������� ������ ��� �����
			 if (!t_number.contains(".") && t_number.compareTo(shortNumber)>=0) 
			 {
					 dictionary.put(t, t_number);
			 }
		 }
			
	 }
	 	
	 	if (newTask && !dictionary.containsValue(shortNumber))
	 	{
	 		//���� ��� ����� ������ � � ������ ��� ��� � ������
	 		return;
	 	}
	 		
	 	for (SecuredTaskBean t: dictionary.keySet())
	 	{
	 			try {
	 				String newNumber=String.format("%02d", Integer.valueOf(dictionary.get(t))+delta);
	 			TaskManager.getTask().updateTask(t.getId(), SafeString.createSafeString(t.getShortname()),
	 					SafeString.createSafeString(t.getName().replaceFirst(getSectionNumber(t), prefix+newNumber)), //�������� ������
	 					SafeString.createSafeString(t.getDescription()),  t.getBudget(), t.getDeadline(), t.getPriorityId(), t.getParentId(), t.getHandlerUserId(), t.getHandlerGroupId(), t.getSubmitdate(), t.getUpdatedate());
	 				}	
	 			catch (NumberFormatException ex1)
	 			{
	 			//������ �������������� �����->������ �� ������
	 				continue;
	 			}
	 	}
	 	
	}
}
