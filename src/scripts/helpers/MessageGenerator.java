package scripts.helpers;

import java.util.ArrayList;
import java.util.Calendar;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.app.adapter.email.change.NewMessageChange;
import com.trackstudio.app.session.SessionContext;
import com.trackstudio.app.session.SessionManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.kernel.cache.UserRelatedManager;
import com.trackstudio.secured.SecuredMessageBean;
import com.trackstudio.secured.SecuredMstatusBean;
import com.trackstudio.secured.SecuredTaskBean;

public class MessageGenerator {

	public static void CreateCommentMsg(SecuredTaskBean task, String message, boolean sendMail) throws GranException {
		
		
		//String messageId=MessageManager.getMessage().createMessage(botId, taskID, mstatusId,
		//		SafeString.createSafeString("��� ��������� ���� ��������� �������������"), null, handlerId, null, null, priorityId, deadline, budget, submitDate);
		SessionContext sc = SessionManager.getInstance().getSessionContext(SessionManager.getInstance().create(UserRelatedManager.getInstance().find(StringConstants.BotId)));
		SessionContext rootsc = SessionManager.getInstance().getSessionContext(SessionManager.getInstance().create(UserRelatedManager.getInstance().find(StringConstants.AdminId)));
		String mstatusId=null;
		String auditId=null;
		ArrayList<SecuredMstatusBean> msList= AdapterManager.getInstance().getSecuredWorkflowAdapterManager().getMstatusList(rootsc, task.getWorkflowId());
		
		
		for (SecuredMstatusBean mstatus:msList)
		{
			
			if (mstatus.getName().contentEquals("�����������"))
			{
				mstatusId=mstatus.getId();
				break;
			}
		}
				
		if (mstatusId==null) 
			{
			TaskCreator.CreateTaskForAdmin("�� ������� �������� �����������", task.getNumber());
			return;
			}
		
		for (SecuredMstatusBean mstatus:msList)
		{
			
			if (mstatus.getName().contentEquals("*"))
			{
				auditId=mstatus.getId();
				break;
			}
		}
		
		String messageId=AdapterManager.getInstance().getSecuredMessageAdapterManager().createMessage(sc,
				task.getId(), mstatusId, message, null, task.getHandlerUserId(), task.getHandlerGroupId(), 
				null, task.getPriorityId(), task.getDeadline(), task.getBudget(), sendMail);
		
		SecuredMessageBean msgBean=AdapterManager.getInstance().getSecuredFindAdapterManager().findMessageById(sc, messageId);
		
		// ������ ����� ��������� ����� ��������, ���� ��������� �����������
		AdapterManager.getInstance().getFilterNotifyAdapterManager().sendNotifyForTask(messageId,
				task.getId(),task.getHandlerUserId(), mstatusId,  
				new NewMessageChange(Calendar.getInstance(), StringConstants.BotId, msgBean, auditId));
		
	}
	
public static void CreateCompleteMsg(SecuredTaskBean task) throws GranException {
		
		
		//String messageId=MessageManager.getMessage().createMessage(botId, taskID, mstatusId,
		//		SafeString.createSafeString("��� ��������� ���� ��������� �������������"), null, handlerId, null, null, priorityId, deadline, budget, submitDate);
		
		String mstatusId=null;
		SessionContext rootsc = SessionManager.getInstance().getSessionContext(SessionManager.getInstance().create(UserRelatedManager.getInstance().find(StringConstants.AdminId)));
		
		ArrayList<SecuredMstatusBean> msgList= AdapterManager.getInstance().getSecuredWorkflowAdapterManager().getMstatusList(rootsc, task.getWorkflowId());
		
		for (SecuredMstatusBean mstatus:msgList)
		{
			if (mstatus.getName().contentEquals("����������"))
			{
				mstatusId=mstatus.getId();
				break;
			}
		}

		if (mstatusId==null) 
		{
			return;
		}
		AdapterManager.getInstance().getSecuredMessageAdapterManager().createMessage(task.getSecure(), task.getId(), mstatusId, "", null, null, null, null,
    			task.getPriorityId(), task.getDeadline(), task.getBudget(), false);
		
	}
	
public static void CreateCloseMsg(SecuredTaskBean task) throws GranException {
	
	
	//String messageId=MessageManager.getMessage().createMessage(botId, taskID, mstatusId,
	//		SafeString.createSafeString("��� ��������� ���� ��������� �������������"), null, handlerId, null, null, priorityId, deadline, budget, submitDate);
	
	String mstatusId=null;
	SessionContext rootsc = SessionManager.getInstance().getSessionContext(SessionManager.getInstance().create(UserRelatedManager.getInstance().find(StringConstants.AdminId)));
	
	ArrayList<SecuredMstatusBean> msgList= AdapterManager.getInstance().getSecuredWorkflowAdapterManager().getMstatusList(rootsc, task.getWorkflowId());
	
	for (SecuredMstatusBean mstatus:msgList)
	{
		if (mstatus.getName().contentEquals("��������"))
		{
			mstatusId=mstatus.getId();
			break;
		}
	}

	if (mstatusId==null) 
	{
		return;
	}
	AdapterManager.getInstance().getSecuredMessageAdapterManager().createMessage(task.getSecure(), task.getId(), mstatusId, "", null, null, null, null,
			task.getPriorityId(), task.getDeadline(), task.getBudget(), false);
	
}


}
