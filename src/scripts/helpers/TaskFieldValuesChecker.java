package scripts.helpers;

import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.trackstudio.exception.GranException;
import com.trackstudio.exception.UserException;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredTaskTriggerBean;


public class TaskFieldValuesChecker {

	public static String getServerDirPath(String path)
	{
		return path.replaceAll("Q:","D:\\\\NetworkDrives\\\\Object").replaceAll("T:","D:\\\\NetworkDrives\\\\Mail")
				.replaceAll("S:", "D:\\\\NetworkDrives\\\\Obmen").replaceAll("R:", "D:\\\\NetworkDrives\\\\Arxiv");
	}
	
	public static String getRemoteDirPath(String serverPath)
	{
		return serverPath.replaceAll("D:\\\\NetworkDrives\\\\Object","Q:").replaceAll("D:\\\\NetworkDrives\\\\Mail","T:")
				.replaceAll("D:\\\\NetworkDrives\\\\Obmen","S:").replaceAll("D:\\\\NetworkDrives\\\\Arxiv","R:");
	}
	
	public static boolean CheckFullPathDir(SecuredTaskTriggerBean task, String errorPrefixMessage)  throws GranException 
	{
		UDFValueAccessor udfAccessor= new UDFValueAccessor();
		ValueContainer vc=udfAccessor.GetAvailableDirPath(task, false); 
	
		if (vc.isExist())			
		{
			
			String globalDirValue=(String)vc.getValue();
			if (globalDirValue.isEmpty()) return true;
			
			if (globalDirValue.contains("Q:") || globalDirValue.contains("R:") || globalDirValue.contains("S:") || globalDirValue.contains("T:"))
			{
				
				Path path= Paths.get(getServerDirPath(globalDirValue));
						
				if (Files.notExists(path) || !Files.isDirectory(path))
				{ 
				//���� ����������� ������� �� �������� - ������� ����� �������������!
				//MessageGenerator.CreateCommentMsg(task.getId(), "���� ����� �������");
					
					throw new UserException("���� "+globalDirValue+" � ����� �� ������"); //�� ���� �����������
				}
			}
			else
			{
				udfAccessor.SetErrorUDFValue(task, globalDirValue +"  - ������� �� ��������� �� ����� �� ������ Q, R, S, T", false);
			}
			return true;
		}
		else
		{
			TaskCreator.CreateTaskForAdmin(errorPrefixMessage+vc.toString(), task.getNumber());
		}
		
		return false;
	}
	
	
}
