package scripts.helpers;

public class ValueContainer {
	static private final ValueContainer  _emptyVC=new ValueContainer();
	static public ValueContainer getEmpty() {return _emptyVC;}
	private Object _value;
	
	public boolean isEmpty()
	{
		return this.equals(_emptyVC);
	}
	
	public Object getValue() {return _value;}
	
	private String _reason;
	//public void setReason(String reason) {_reason=reason;}
	public String getReason() {return _reason;}
	
	private boolean _isExist;
	public boolean isExist() {return _isExist;}
	
	private String _udfName;
	
	public String getUDFName() {return _udfName;}
	
	private ValueContainer()
	{
		_value="Empty";
		_isExist=false;
		_udfName="";
	}
	
	public ValueContainer(Object value, String udfName)
	{
			_value=value;
			_isExist=(value==null)? false:true;
			_udfName=udfName;
	}
	public ValueContainer(String reason, String udfName)
	{
		_value=null;
		_isExist=false;
		_reason=reason;
		_udfName=udfName;
		
	}
	
	@Override
	public String toString()
	{
		if (_isExist)
		{ return "Value of "+_udfName+"="+_value;}
		return "Value of "+_udfName+"=null, Reason="+_reason;
	}
}
