package scripts.helpers;

import com.trackstudio.app.Slider;
import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.app.session.SessionContext;
import com.trackstudio.app.session.SessionManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.kernel.cache.UserRelatedManager;
import com.trackstudio.secured.SecuredTaskBean;

public class TaskSearcher {

	public static Slider<SecuredTaskBean> GetTaskListForUser(String userId, String filterId, String taskId) throws GranException		{
	 SessionContext sc = SessionManager.getInstance().getSessionContext(SessionManager.getInstance().create(UserRelatedManager.getInstance().find(userId)));
	 return AdapterManager.getInstance().getSecuredTaskAdapterManager().getTaskList(sc, taskId, filterId, true, 100, null);
	}
	 
}
