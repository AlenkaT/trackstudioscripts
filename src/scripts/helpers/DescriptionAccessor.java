package scripts.helpers;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.secured.SecuredMessageTriggerBean;

public class DescriptionAccessor {

	public static void AppendMessageDescription(SecuredMessageTriggerBean arg0, String message)
	{
		arg0.setDescription(arg0.getDescription()+ "<br>"+
				"��� ��������� ��������� �������� ��:<br>" +message);
	}
	
	public static void SetMessageDescriptionByAdapterManager(SecuredMessageTriggerBean arg0, String message) throws GranException
	{
		String newMessage= arg0.getDescription()+ "<br>"+"��� ��������� ��������� �������� ��:<br>" +message;
		AdapterManager.getInstance().getSecuredMessageAdapterManager().updateDescription(arg0.getSecure(), arg0.getId(), newMessage);	
	}
	
	
	
}
