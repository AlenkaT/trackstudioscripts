package scripts.helpers;


import java.util.Date;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.app.session.SessionContext;
import com.trackstudio.app.session.SessionManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.kernel.cache.UserRelatedManager;
import com.trackstudio.kernel.manager.SafeString;


public class TaskCreator {
	public static void CreateTaskForAdmin (String message, String sourceTaskNumber) throws GranException
	{
		SessionContext sc = SessionManager.getInstance().getSessionContext(SessionManager.getInstance().create(UserRelatedManager.getInstance().find(StringConstants.BotId)));	
		String parentTaskId=AdapterManager.getInstance().getSecuredTaskAdapterManager().findTaskByNumber(sc, "853").getId();	
		String name="AutoBug of task #"+sourceTaskNumber;
		
		String taskId=AdapterManager.getInstance().getSecuredTaskAdapterManager().createTask(sc, parentTaskId, CategoryIDs.Bug, name);
		String priorityId="402881816ff10179016ff56cce860158";
	
		
		SafeString description= SafeString.createSafeString(new Date().toString()).append("<br>").append(message);
		if (sourceTaskNumber!=null) 
		{
			description.append("<br>").append("SourceTask:#"+sourceTaskNumber);
		}
		AdapterManager.getInstance().getSecuredTaskAdapterManager().updateTask(sc, taskId, "bug", name, description.toString(), null, null,priorityId , parentTaskId, StringConstants.AdminId, null, true);
	}
	
	public static void CreateTaskForAdminAboutException(String methodName, String className, String message, String sourceTaskNumber, Exception ex) throws GranException {
	
		TaskCreator.CreateTaskForAdmin(message+"��� ���������� ������ "+methodName+" ������ "+className+" �������� ����������: <br>"+
				"ExceptionType: "+ ex.getClass().getName()+"<br>"+
				"Message: "+ex.getMessage()+"<br>" +
				"StackTrace: "+ex.getStackTrace(), sourceTaskNumber);
	}

}
