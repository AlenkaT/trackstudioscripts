package scripts.before_add_message;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Scanner;

import com.trackstudio.app.TriggerManager;
import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.app.adapter.email.change.NewMessageChange;
import com.trackstudio.app.session.SessionContext;
import com.trackstudio.app.session.SessionManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.exception.UserException;
import com.trackstudio.external.OperationTrigger;
import com.trackstudio.kernel.cache.UserRelatedManager;
import com.trackstudio.kernel.manager.MessageManager;
import com.trackstudio.kernel.manager.SafeString;
import com.trackstudio.kernel.manager.TaskManager;
import com.trackstudio.kernel.manager.UdfManager;
import com.trackstudio.kernel.manager.UserManager;
import com.trackstudio.secured.SecuredMessageBean;
import com.trackstudio.secured.SecuredMessageTriggerBean;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredUDFBean;
import com.trackstudio.secured.SecuredUserBean;

public class LoadDocumentation implements OperationTrigger {
	private SessionContext sc=null;
	private String udfGlobalDirId="402881816f228b86016fb354e39f30e7"; //id ���� Global Dir ��� �����
	private String udfNumberId; //���������� �����
	private String categoryId;
	private String priorityId;
	private String mstatusId1;
	private String mstatusId2;
	private String handlerUserId;
	private String parentId;
	private Calendar deadline;
	private long budget;
	
	private String CreateTask(String name, String globalPathStr, String number) throws GranException
	{
		String newtaskID= AdapterManager.getInstance().getSecuredTaskAdapterManager().createTask(sc, parentId, categoryId, name);
		String messageID1=MessageManager.getMessage().createMessage(sc.getUserId(), newtaskID, mstatusId1,
				SafeString.createSafeString(""), null, handlerUserId, null, null, priorityId, deadline, budget, Calendar.getInstance());
		String messageID2=MessageManager.getMessage().createMessage(handlerUserId, newtaskID, mstatusId2,
				SafeString.createSafeString(""), null,sc.getUserId() , null, null, priorityId, deadline, budget, Calendar.getInstance());
		AdapterManager.getInstance().getSecuredUDFAdapterManager().setTaskUdfValue(sc, udfGlobalDirId , newtaskID, globalPathStr);			
		AdapterManager.getInstance().getSecuredUDFAdapterManager().setTaskUdfValue(sc, udfNumberId , newtaskID, number);
		
		return newtaskID;
	}
	
	@Override
	//��������� ������ ������� �� ���������� �����
	public SecuredMessageTriggerBean execute(SecuredMessageTriggerBean msg) throws GranException {

		SecuredTaskBean task = msg.getTask();
		sc=SessionManager.getInstance().getSessionContext(SessionManager.getInstance().create(UserRelatedManager.getInstance().find(task.getHandlerUserId())));
					
		SecuredUDFBean udfGlobalDir = AdapterManager.getInstance().getSecuredFindAdapterManager().findUDFById(task.getSecure(), udfGlobalDirId);
		String globalDirValue=((String)task.getUdfValueByName(udfGlobalDir.getCaption()));
		
		if (globalDirValue==null)
		{
			throw new UserException("�� ������� �������� ������ � ���� GlobalDir");
		}
		
		String globalPathStr=globalDirValue.replaceAll("Q:","D:\\\\NetworkDrives\\\\Object").replaceAll("T:","D:\\\\NetworkDrives\\\\Mail").replaceAll("S:", "D:\\\\NetworkDrives\\\\Obmen").replaceAll("R:", "D:\\\\NetworkDrives\\\\Arxiv");
		
		if (globalPathStr.isEmpty()) 
		{msg.setDescription("�� ������ ���� � ����� ������������ � ���� GlobalDir"); return msg;}
		
		
		Path globalPath= Paths.get(globalPathStr);
		if (Files.notExists(globalPath) || !Files.isDirectory(globalPath))
		{ 
			throw new UserException("�����, �� ����, ������� ������ � ���� GlobalDir �� ����������");
		}
		
		
		parentId=task.getId();
		deadline=task.getDeadline();
		budget=task.getBudget();
		handlerUserId=AdapterManager.getInstance().getSecuredUserAdapterManager().findUserIdByQuickGo(task.getSecure(), "Bukhantsova_OI");
				
		
		File dir = new File(globalPathStr); //path ��������� �� ����������
		File[] arrFiles = dir.listFiles();
		List<File> lstFSEntities = Arrays.asList(arrFiles);
		String fileName;
		String dirName="";
		String newtaskID;
		String number="";
		for (File f: arrFiles)
		{
			fileName=f.getName();
			
			
			if (f.isDirectory())
			{
				dirName="\\"+fileName;
				//����� ������� ������ � ���������� ����� ����
				File[] arrFiles2 = f.listFiles();
				//���� ��� ����������
				continue;
			}
			else
			{
				dirName="";
			}
			
			if (fileName.substring(0,2).matches("\\d"))
			{
				number= fileName.substring(0,2);
			}
			
			globalDirValue=globalDirValue+dirName;
			if (fileName.endsWith(".dwg"))
			{
				String newtaskName= fileName.replaceAll(".dwg", "");
				//��� ������, ������ ������ ��� ������
				//����� ��������
					categoryId="4028808a1951e21b011952bddb38032c";
					priorityId="4028808a1951e21b011952b524ff0288";//�������
					//statusId="4028808a1951e21b011952b5250e0290" ;// ��������+		
					//mstatusId="4028808a1951e21b011952b5258b02bf"; //���������
					mstatusId1="4028808a1951e21b011952b5253d02a4";// ������ � ������
					mstatusId2="402881816f228b86016f36e49c250485";// ������ �� ��������
					udfNumberId="402881816f228b86016f460fbc2509af";
					newtaskID = CreateTask(newtaskName,globalDirValue,number) ;
				continue;
			}
			
			if (fileName.endsWith(".doc") || fileName.endsWith(".docx"))
			{
				String newtaskName= fileName.replaceAll(".doc", "").replaceAll(".docx", "");
				//��� ��� �� ���������
				
				priorityId="402881816ff5b7bf016ffb3877cb0867";
				//statusId="402881816ff5b7bf016ffb3877cb0873";
				//mstatusId1="402881816ff5b7bf016ffb3877f9089a";
				udfNumberId="4028818171f2f7ba017212941a8815ff";
				if (newtaskName.contains("��") || newtaskName.contains("������������� �������"))
				{
					categoryId="402881816ff5b7bf017056f2f73e1c41";
					newtaskID = CreateTask(newtaskName,globalDirValue, number) ;
					continue;
				}
				if (newtaskName.contains("���") || newtaskName.contains("���������") || newtaskName.contains("������������"))
				{
					categoryId="402881816f228b86016fb2c409882aca";
					newtaskID = CreateTask(newtaskName,globalDirValue, number) ;
					continue;				
				}
				if (newtaskName.contains("�����"))
				{
					categoryId="4028818171f2f7ba01721226af3912bf"; 
					newtaskID = CreateTask(newtaskName,globalDirValue,number) ;
					continue;				
				}
				if (newtaskName.contains("������"))
				{
					categoryId="4028808a1951e21b01195245ff4200c1";
					newtaskID = CreateTask(newtaskName,globalDirValue,number) ;
					continue;				
				}
				
				categoryId="4028818171f2f7ba0172121c47081149";
				newtaskID = CreateTask(newtaskName,globalDirValue,number) ;
				continue;
			}
			
			if (fileName.endsWith(".xls") || fileName.endsWith(".xlsx"))
			{
				String newtaskName= fileName.replaceAll(".xls", "").replaceAll(".xlsx", "");
				//��� ����� �� �������
				categoryId="4028808a1951e21b01195245ff4200c1";
				priorityId="402881816ff5b7bf016ffb3877cb0867";
				//statusId="402881816ff5b7bf016ffb3877cb0873";
			//	mstatusId="402881816ff5b7bf016ffb3877f9089a";
				newtaskID = CreateTask(newtaskName,globalDirValue,number) ;
				continue;
			}
			
			
			
		}
			
			
			
/*
		
			
			
			
			if (udfID1!=null)
			{			
				AdapterManager.getInstance().getSecuredUDFAdapterManager().setTaskUdfValue(sc, udfID1 , newtaskID, udf1Value);
			}
			if (udfID2!=null)
			{
				SecuredUDFBean UDFParticipants = AdapterManager.getInstance().getSecuredFindAdapterManager().findUDFById(sc, udfID2);
				AdapterManager.getInstance().getSecuredUDFAdapterManager().setTaskUDFValueSimple(sc, newtaskID, UDFParticipants.getCaption(),  udf2Value);
				//AdapterManager.getInstance().getSecuredUDFAdapterManager().setTaskUdfValue(sc, udfID2 , newtaskID, udf2Value);
			}
			if (udfID3!=null)
			{
				AdapterManager.getInstance().getSecuredUDFAdapterManager().setTaskUdfValue(sc, udfID3 , newtaskID, udf3Value);		
			}
		
        }
        
        
		catch (Exception ex)
        {
        	msg.setDescription("udf2Value="+udf2Value+"  "+ex.getMessage());
        }
        } //end for
		
		
		
		*/

	    return msg;
	    	
	}
	
}
