package scripts.before_add_message;

import java.util.ArrayList;
import java.util.Calendar;

import com.trackstudio.app.adapter.AdapterManager;

import com.trackstudio.exception.GranException;

import com.trackstudio.external.OperationTrigger;

import com.trackstudio.secured.SecuredMessageTriggerBean;

import com.trackstudio.secured.SecuredUDFBean;
import com.trackstudio.secured.SecuredUserBean;

import scripts.helpers.TaskCreator;
import scripts.helpers.DescriptionAccessor;
import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.ValueContainer;
import scripts.task_custom_field_value.AlreadyReadUsersCalculator;


public class ResendLetter implements OperationTrigger {
	
	
	@Override
	public SecuredMessageTriggerBean execute(SecuredMessageTriggerBean arg0) throws GranException 
	{
		try
		{
			UDFValueAccessor udfAccessor= new UDFValueAccessor();
		//������� �������� ���� �������� �����������, ������ ��������� ����� ���� �� ������� �������	

		//�������� ���� "���������� ���������"
		ValueContainer vc = udfAccessor.GetUDFValue( UDF_IDs.NeedToSend, arg0, false);
		
		if (!vc.isExist())
		{
			DescriptionAccessor.AppendMessageDescription(arg0, "�������� ��������� � �������� ��-�� ���� ��� ���������� �������");
			TaskCreator.CreateTaskForAdmin("�������� ���� ���������� ��������� ��� ���������� �������� ������������� ������������� @"+arg0.getSubmitter().getLogin()+" �� ���� ��������, ����������� ���������� ������� ����������", arg0.getTask().getTaskNumber());
			return arg0;
		}
		
/*
		if (subscribedUsersValue==null) // � �������� ��� �����������, ������������ ������ ������ (���� �� �������� ��� ������ - ��� ����� ����� - null, ����� null ������������, ���� ���� ������� �����������!)
	    {
	    	//���� �� �������� ��� �������������� � ������ ��������
	    	arg0.setDescription("�������� ���� ���������� ��������� �� �������� ��� ������������ ���������� ����� ��������. �������� �������������� TrackStudio");
	    	return arg0;
	    }
	    */
	    String submitter = "@"+arg0.getSubmitter().getLogin();
		String handler= arg0.getHandler().getLogin();
		
		
		
		
		
		/*
		//����������� �������� ��������� � ������ �����������
		if (!readUsersValue.contains(submitter))
		{
		   if (!readUsersValue.isEmpty())
		   {
			   readUsersValue = readUsersValue+";";// ��������� ����������� ��������
		   }
		   readUsersValue=readUsersValue+submitter;
		   arg0.setUdfValue(udfReadUsers.getCaption(), readUsersValue); // �������� �������� ���� "��������"		    				
		}
		*/
		

		 String subscribedUsersValue=((String)vc.getValue()).replaceAll(submitter, ";" ); //������ �������� ����������� �� ���������� ���������
		
		
		//� ������ ����� �������������� , ������ �������������� ������� � ���� ���������� ���������, ����� ��������� ������� ������ "������, ������� ��� ����� ���������"
	    if (!((String)vc.getValue()).contains(handler) ) //�������� ��� ����������� ����� �������� �� �� ���� 
	    {
	    	//��������� �������������� � ������ ���, ���� ����� ��������� ������
	    	ArrayList<String> readUsersValue = AlreadyReadUsersCalculator.GetAlreadyReadUsers(arg0.getTask());
			if (readUsersValue==null || !readUsersValue.contains(handler))
			{
	    	//	���� ������ Empty ���� �� ����� � ���������� ���������� beforeCreateTask ��� ������
				subscribedUsersValue=subscribedUsersValue+";"+"@"+handler;
			}
	    }
	    
	    
	    
	    arg0.setUdfValue(vc.getUDFName(), subscribedUsersValue); // �������� �������� ���� "���������� ���������"
	    /* ��������� ������� �����������, ��� ��� ��������� ������ � ���������� �������� � ��������� ������� �������� � ������ �������
	       �� �������������*/
	    
	 		
		
		}
		catch (Exception ex)
		{
			// ������ ��� ��������� �������� �������� ��������
			DescriptionAccessor.AppendMessageDescription(arg0, "��� ���������� �������� �������� ����������: "+ ex.getMessage()+"\n �������� �������������� TrackStudio ");
			TaskCreator.CreateTaskForAdminAboutException("execute", this.getClass().getName(),"���������� �������� ������������� ������������� @"+arg0.getSubmitter().getLogin(), arg0.getTask().getTaskNumber(), ex);
			
		}
		
	    return arg0;
	}

}
		
