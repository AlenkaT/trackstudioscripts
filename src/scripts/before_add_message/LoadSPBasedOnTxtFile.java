package scripts.before_add_message;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Scanner;

import com.trackstudio.app.TriggerManager;
import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.app.adapter.email.change.NewMessageChange;
import com.trackstudio.app.session.SessionContext;
import com.trackstudio.app.session.SessionManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.OperationTrigger;
import com.trackstudio.kernel.cache.UserRelatedManager;
import com.trackstudio.kernel.manager.MessageManager;
import com.trackstudio.kernel.manager.SafeString;
import com.trackstudio.kernel.manager.TaskManager;
import com.trackstudio.kernel.manager.UdfManager;
import com.trackstudio.kernel.manager.UserManager;
import com.trackstudio.secured.SecuredMessageBean;
import com.trackstudio.secured.SecuredMessageTriggerBean;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredUDFBean;
import com.trackstudio.secured.SecuredUserBean;

public class LoadSPBasedOnTxtFile implements OperationTrigger {
	
	@Override
	//��������� ������ ������� �� ���������� �����
	public SecuredMessageTriggerBean execute(SecuredMessageTriggerBean msg) throws GranException {

		SecuredTaskBean task = msg.getTask();
		
		
		String udfGlobalDirId="402881816f228b86016fb354e39f30e7"; //id ���� Global Dir ��� �����			
		SecuredUDFBean udfGlobalDir = AdapterManager.getInstance().getSecuredFindAdapterManager().findUDFById(task.getSecure(), udfGlobalDirId);
		
		String globalDirValue_r=((String)task.getUdfValueByName(udfGlobalDir.getCaption()));
		
		String globalDirValue=globalDirValue_r.replaceAll("Q:","D:\\\\NetworkDrives\\\\Object").replaceAll("T:","D:\\\\NetworkDrives\\\\Mail").replaceAll("S:", "D:\\\\NetworkDrives\\\\Obmen").replaceAll("R:", "D:\\\\NetworkDrives\\\\Arxiv");
		
		
		FileReader fr;
		List<String> arr = new ArrayList<String>();
		try 
		{
			fr = new FileReader(globalDirValue+"\\BuildingHelper.txt" );
			
			Scanner scan = new Scanner(fr); 
	        int i=0;
	        while (scan.hasNextLine()) 
	        {
	        	if (i>0)
	        	{  
	        		boolean answer= arr.add(scan.nextLine());
	        	}
	        	else
	        	{
	        		scan.nextLine();
	        	}
	        	i=i+1;
	        }
	        scan.close();
	        fr.close();
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			msg.setDescription(e.getMessage());
		}
		
		
		
		
        

        int number=0;
        
		for(String str:arr)
		{

	        String parentId=null;
	        String categoryId="null";
	        String  mstatusId=null;
	        String udfID1=null;
	        String udf1Value="";
	        String udfID2=null;
	        String udf2Value="";
	        String udfID3=null;
	        String udf3Value="";
	        SafeString shortname=null;
	        String name=null;
	        String prefix="";
	        String description=null;
	        String priorityId="null";
	        String handlerId=null;
	       
	        String userID=null;
	        String statusId=null;
	        String resolutionId=null;
	        Long budget = Long.valueOf(0);
	        Calendar deadline=null;
	        Calendar submitDate=null;
	        Calendar updateDate=null;
	        Calendar closeDate=null;
	        SecuredMessageTriggerBean newMsg=null; 
		try
	    {
			number++;
			String[] values= str.split("\\t");
			
			if (values.length<13) continue;
			
			parentId=task.getId();
			
			handlerId=UserManager.getUser().findUserIdByQuickGo(values[0]);
			
			String[] values2 = values[2].split(";");
			

			if (values[1].contentEquals("������"))
			{
				
					categoryId="402881816f228b86016ff05551bb39cd";
					switch (values[11])
					{
						case "end": // statusId="402881816f228b86016ff0499923390f" ;// ��������
									mstatusId="402881816f228b86016ff04999263911";
									handlerId=null; // �������������
									break;
						case "new": 
									//statusId="402881816f228b86016ff04999233910";  //�������
									mstatusId="4028818172e0c3d40172e1d2838709bf"; //�������� ���������������
									break;
						case "inProgress": //	statusId=	"402881816f228b86016ff04a7cfb395a"; //� ��������
									mstatusId=	"402881816f228b86016ff04c977d395b"; //�������� �����
									break;
						case "stop": //statusId="402881816ff5b7bf017053ad9f941b4d"; // ����������
									mstatusId="402881816ff5b7bf017053ae70511b79";
									break;
					
						default: //statusId="402881816f228b86016ff04999233910";
									mstatusId="4028818172e0c3d40172e1d2838709bf";
					}
					
					priorityId="402881816f228b86016ff0499921390e"; //��� ������� �� ������ (������ ����������)				 
					
					if(values2.length>0)
					{
						udfID1="402881816f228b86016ff05306db39ae"; // �����������
						udf1Value=values2[0];
					}
					if(values2.length>1)
					{
						udfID2="402881816ff5b7bf01709a85122f2936"; // ����� ����
						udf2Value=values2[1];
					}
					
					//break;
			}
			
			if (values[1].contentEquals("������"))
			{
					categoryId="4028808a1951e21b011952bddb38032c";
					switch (values[7])
					{
						case "1": priorityId="4028808a1951e21b011952b5250e028c";  	break; //��. ������
						case "2": priorityId="4028808a1951e21b011952b524ff0287";  	break; //������
						case "3": priorityId="4028808a1951e21b011952b524ff0288"; 	break;//�������
						case "4": priorityId="4028808a1951e21b011952b5250e0289";	break; // �������
						case "5": priorityId="4028808a1951e21b011952b5250e028a";	break; // ��.�������
						case "6": priorityId="4028808a1951e21b011952b5250e028b";	break; // ��������������
						default:  priorityId="4028808a1951e21b011952b524ff0288"; 	break;//�������
					}
					//
					
				/*	switch (values[11])
					{
						case "plan": 		statusId="4028808a1951e21b011952b5250e028d"; break; //������+
						case "end": 		statusId="4028808a1951e21b011952b5250e0290" ;break;// ��������+
						case "obsolete": 	statusId="402881816f228b86016f369c5434043d";break;// ��������+
						case "start": 		statusId="402881816f228b86016f3bac7fa40754";break;  //�������+
						case "check": 		statusId="4028808a1951e21b011952b5250e028f";break;  //��������+
						case "inProgress": 	statusId="4028808a1951e21b011952b5250e028e"; break; //� ��������+
						case "bug" : 		statusId="402881816f228b86016f369e8832043e"; break; //������� ������+
						case "stop": 		statusId="4028808a1951e21b011952b5250e0291";break; // ����������+
						default: statusId="4028808a1951e21b011952b5250e028d";
					}*/
					
					switch (values[11])
					{
						/*case "plan": 		mstatusId="4028808a1951e21b011952b5250e028d"; break; //������+
						case "end": 		mstatusId="4028808a1951e21b011952b5250e0290" ;break;// ��������+
						case "obsolete": 	mstatusId="402881816f228b86016f369c5434043d";break;// ��������+
						 */case "start": 		mstatusId="4028808a1951e21b011952b5253d02a4";break;  // ������������� ������������ ������ ���������� � ������
					/*	case "check": 		mstatusId="4028808a1951e21b011952b5250e028f";break;  //��������+
						case "inProgress": 	mstatusId="4028808a1951e21b011952b5250e028e"; break; //� ��������+
						case "bug" : 		mstatusId="402881816f228b86016f369e8832043e"; break; //������� ������+
						case "stop": 		mstatusId="4028808a1951e21b011952b5250e0291";break; // ����������+*/
						default: statusId="4028808a1951e21b011952b5250e028d";
					}
					
					if (values[13].contentEquals("Y"))
					{
						udfID1="402881816f228b86016f460fbc2509af"; //���������� �����
						if (values.length>=15)
						{
							//��������� ���������� ������
							number=Integer.valueOf(values[14]);
						}
						udf1Value=String.valueOf(number);
						prefix=String.format("%02d", number)+"_";
					}
						else
						{
							prefix=values[13]+"_";
						}
					
					if(values2.length>0)
					{
						udfID2="4028818171f2f7ba0171fdce63ea04e8"; //������ ����������
						String[] list=values2[0].split(",");
						for (String login:list )
							{
								//�� ������� ���������� ������������, ������ ���������� ����������� �������
								
								if (UserManager.getUser().findUserIdByQuickGo(login)!=null)
								{
									
									udf2Value=udf2Value+login+";";
								}
						}							
					}
					
				}
			if (values[1].contentEquals("���������") ||values[1].contentEquals("��") || values[1].contentEquals("������")
					|| values[1].contentEquals("�����") || values[1].contentEquals("�����"))
			{
				//��������� (���������� � ��������� ������� �� ��������)
				//, "��", "������", "�����", "�����"
				switch (values[1])
				{
					case "���������":  categoryId="402881816f228b86016fb2c409882aca"; break; //+
					case "��": categoryId="402881816ff5b7bf017056f2f73e1c41"; break; //+
					case "�����": categoryId="4028818171f2f7ba01721226af3912bf"; break; //+
					case "�����": categoryId="4028818171f2f7ba0172121c47081149"; break; //+
					case "������":  categoryId="4028808a1951e21b01195245ff4200c1"; break; //+
					default: categoryId="4028818171f2f7ba0172121c47081149"; break; //�����
				}
				
				switch (values[7]) // ��������� � ��������
					{
						case "1": priorityId="402881816ff5b7bf016ffb3877cb086b";  break; //��. ������
						case "2": priorityId="402881816ff5b7bf016ffb3877cb0866";  break; //������
						case "3": priorityId="402881816ff5b7bf016ffb3877cb0867";  break;//�������
						case "4": priorityId="402881816ff5b7bf016ffb3877cb0868"; break; // �������
						case "5": priorityId="402881816ff5b7bf016ffb3877cb0869"; break; // ��.�������
						case "6": priorityId="402881816ff5b7bf016ffb3877cb086a"; break; // ��������������
						default:  priorityId="402881816ff5b7bf016ffb3877cb0867";  break;//�������
					}
					//
					
				switch (values[11]) // ��������� � ��������
					{
						case "plan": 		statusId="402881816ff5b7bf016ffb3877cb0870"; break; //������+
						case "end": 		statusId="402881816ff5b7bf016ffb3877cb0873" ;break;// ��������+
						case "obsolete": 	statusId="402881816ff5b7bf016ffb3877cb086c";break;// ��������+
						case "start": 		statusId="402881816ff5b7bf016ffb3877cb0871";break;  //�������+
						case "check": 		statusId="402881816ff5b7bf016ffb3877cb086d";break;  //��������+
						case "inProgress": 	statusId="402881816ff5b7bf016ffb3877cb0872"; break; //� ��������+
						case "bug" : 		statusId="402881816ff5b7bf016ffb3877cb086e"; break; //������� ������+
						case "stop": 		statusId="402881816ff5b7bf016ffb3877cb086f";break; // ����������+
						
					
						default: statusId="402881816ff5b7bf016ffb3877cb0870";
					}
					
				if (values[13].contentEquals("Y"))
				{
					udfID1="4028818171f2f7ba017212941a8815ff"; //���������� �����
					if (values.length>=15)
					{
						//��������� ���������� ������
						number=Integer.valueOf(values[14]);
					}
					udf1Value=String.valueOf(number);
					prefix=String.format("%02d", number)+"_";
				}
					else
					{
						prefix=values[13]+"_";
					}
				
				 if(values2.length>0)
					{
						udfID2="4028818171f2f7ba017212930b3f15c8"; //������ ����������
						String[] list=values2[0].split(",");
						for (String login:list )
							{
								//�� ������� ���������� ������������, ������ ���������� ����������� �������
								
								if (UserManager.getUser().findUserIdByQuickGo(login)!=null)
								{
									
									udf2Value=udf2Value+login+";";
								}
						}							
					}
					
			}
			if (values[1].contentEquals("�����"))
			{
				categoryId="402881816f228b86016f9df90a0517e4"; 
				statusId="2";
				priorityId="2";
				
			}	
			
			name=prefix+values[3];
			userID=UserManager.getUser().findUserIdByQuickGo(values[8]);
			if (userID==null)
			{
				userID=task.getSubmitterId();
			}
			
			//msg.setDescription("parentId "+parentId +", userID "+userID+", categoryId "+categoryId+
			//		 ", name" + name +", deadline " + deadline +", submitDate " +submitDate +", updateDate "+updateDate+", statusId "+statusId);
			//������� ����� ������
			//String newtaskID =TriggerManager.getTask().createTask(parentId, userID, categoryId, SafeString.createSafeString(name), deadline, submitDate, updateDate);
			SessionContext sc= SessionManager.getInstance().getSessionContext(SessionManager.getInstance().create(UserRelatedManager.getInstance().find(userID)));
			String newtaskID =AdapterManager.getInstance().getSecuredTaskAdapterManager().createTask(sc, parentId, categoryId, name);
			description=values[4];
			
			Long t5=Long.valueOf(values[5]);
			if (t5>0)
			{
				budget=t5;
			}

			
			String[] date1= values[6].split(".");
			
			if (date1.length!=3)
			{
					deadline=task.getDeadline();
			}
			else
			{
				deadline= new GregorianCalendar(Integer.valueOf(date1[2]),Integer.valueOf(date1[1]),Integer.valueOf(date1[0]));
			}
			String[] date= values[9].split(".");
			if (date.length!=3)
			{
				submitDate=task.getSubmitdate();  //9 �� ������}
			}
			else
			{
				submitDate = new GregorianCalendar(Integer.valueOf(date[2]),Integer.valueOf(date[1]),Integer.valueOf(date[0]));
			}
			String[] date2= values[10].split(".");
			if (date2.length!=3)
			{
				updateDate=task.getUpdatedate();  //10 �� �������
			}
			else
			{
				updateDate = new GregorianCalendar(Integer.valueOf(date2[2]),Integer.valueOf(date2[1]),Integer.valueOf(date2[0]));
			}
			
			
			
			/*TaskManager.getTask().updateTask(newtaskID , shortname, SafeString.createSafeString(name)
    				,SafeString.createSafeString(description), budget, deadline, priorityId, parentId, handlerId, task.getHandlerGroupId(), 
    				submitDate, updateDate);
    				*/	
			/*AdapterManager.getInstance().getSecuredTaskAdapterManager().updateTask(sc, newtaskID, task.getShortname(), name, description,
					budget, deadline, priorityId, parentId, handlerId, task.getHandlerGroupId(), true, submitDate, updateDate);
			*/
			
			String messageId=MessageManager.getMessage().createMessage(handlerId, newtaskID, mstatusId, SafeString.createSafeString(""), null, handlerId, null, null, priorityId, deadline, budget, submitDate);
			/*String messageId=AdapterManager.getInstance().getSecuredMessageAdapterManager().createMessage(sc, newtaskID, mstatusId, "", 0l, handlerId,
						null, null, priorityId, deadline, budget, true);*/
			
			AdapterManager.getInstance().getFilterNotifyAdapterManager().sendNotifyForTask(messageId, newtaskID, userID, mstatusId,  new NewMessageChange(Calendar.getInstance(), handlerId, new SecuredMessageBean(messageId, sc), null));
			
			
			//������ ��� �� �������, ���� values[12] 1
			if (values[12].contentEquals("Y"))
			{
				Path newDirValue=Paths.get(globalDirValue+"\\"+name); 
				try
				{
					Path newDirPath=Files.createDirectories(newDirValue);
					AdapterManager.getInstance().getSecuredUDFAdapterManager().setTaskUdfValue(sc, udfGlobalDirId , newtaskID, globalDirValue_r+"\\"+name);
				}
				catch (IOException ex)
				{
				//������������� �� ��������
				//�� ��������,���� �� ����� �� ������� ������� �� ������
				// ������ �. ���������  name �� ���������� ������������ �������� � ��������� �� ��� �������� ��������
				}
			}
			if (values[12].contentEquals("N"))
			{
				
				AdapterManager.getInstance().getSecuredUDFAdapterManager().setTaskUdfValue(sc, udfGlobalDirId , newtaskID, globalDirValue_r);
			}
			
			if (udfID1!=null)
			{			
				AdapterManager.getInstance().getSecuredUDFAdapterManager().setTaskUdfValue(sc, udfID1 , newtaskID, udf1Value);
			}
			if (udfID2!=null)
			{
				SecuredUDFBean UDFParticipants = AdapterManager.getInstance().getSecuredFindAdapterManager().findUDFById(sc, udfID2);
				AdapterManager.getInstance().getSecuredUDFAdapterManager().setTaskUDFValueSimple(sc, newtaskID, UDFParticipants.getCaption(),  udf2Value);
				//AdapterManager.getInstance().getSecuredUDFAdapterManager().setTaskUdfValue(sc, udfID2 , newtaskID, udf2Value);
			}
			if (udfID3!=null)
			{
				AdapterManager.getInstance().getSecuredUDFAdapterManager().setTaskUdfValue(sc, udfID3 , newtaskID, udf3Value);		
			}
		
        }
        
        
		catch (Exception ex)
        {
        	msg.setDescription("udf2Value="+udf2Value+"  "+ex.getMessage());
        }
        } //end for
		
		
		
		

	    return msg;
	    	
	}
	
}
