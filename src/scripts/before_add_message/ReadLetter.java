package scripts.before_add_message;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.app.session.SessionContext;
import com.trackstudio.app.session.SessionManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.exception.UserException;
import com.trackstudio.external.OperationTrigger;
import com.trackstudio.kernel.cache.UserRelatedManager;
import com.trackstudio.kernel.manager.MessageManager;
import com.trackstudio.kernel.manager.TaskManager;
import com.trackstudio.secured.SecuredMessageTriggerBean;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredUDFBean;
import com.trackstudio.secured.SecuredUserUDFBean;
import com.trackstudio.securedkernel.SecuredUDFAdapterManager;
import com.trackstudio.tools.Pair;
import com.trackstudio.tools.textfilter.HTMLEncoder;

import scripts.helpers.TaskCreator;
import scripts.helpers.DescriptionAccessor;
import scripts.helpers.RequestGenerator;
import scripts.helpers.SimpleLogger;
import scripts.helpers.StringConstants;
import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.UserUDF_IDs;
import scripts.helpers.ValueContainer;

public class ReadLetter implements OperationTrigger {

	
	@Override
	public SecuredMessageTriggerBean execute(SecuredMessageTriggerBean arg0) throws GranException 
	{
		try
		{
			UDFValueAccessor udfAccessor= new UDFValueAccessor();
		  	// ��������� ������ �� �������� � ����������			
			 new RequestGenerator().CreateOpenExplorerRequest(arg0);

			//������� �������� ���� �������� �����������, ������ ��������� ����� ���� �� ������� �������
			
			
			//�������� ���� "���������� ���������"
			
			ValueContainer vc = udfAccessor.GetUDFValue( UDF_IDs.NeedToSend, arg0, false);
			
			if (!vc.isExist())
			{
				DescriptionAccessor.AppendMessageDescription(arg0, "�������� ��������� � �������� ��-�� ���� ��� ���������� �������");
				TaskCreator.CreateTaskForAdmin("�������� ���� ���������� ��������� ��� ���������� �������� ��������� ������������� @"+arg0.getSubmitter().getLogin()+" �� ���� ��������, ����������� ���������� ������� ����������"+vc.getReason(), arg0.getTask().getTaskNumber());
				return arg0;
			}
			
			//������ �������� ����������� �� ���������� ���������
			 String submitter = "@"+arg0.getSubmitter().getLogin();
			 String subscribedUsersValue=((String)vc.getValue()).replaceAll(submitter, ";" );
			
		    
		    arg0.setUdfValue(vc.getUDFName(), subscribedUsersValue); // �������� �������� ���� "���������� ���������"
		    //�� ����������� �� ���� ��������
		    //AdapterManager.getInstance().getSecuredUDFAdapterManager().setTaskUdfValue(scAdm,UDFSubscribedUsersID , arg0.getTaskId(), subscribedUsersValue);
		    
		    //!!!!!!!!!!� �������� �������� ��������� ����������� ��������� ��������������!!!!!!!!!!!!!
		    /*	
		   if (!readUsersValue.contains(submitter))
		   {
		    		if (!readUsersValue.isEmpty())
		    		{
		    			readUsersValue = readUsersValue+";";// ��������� ����������� ��������
		    		}
		    		readUsersValue=readUsersValue+submitter;
		    		
		    		arg0.setUdfValue(udfReadUsers.getCaption(), readUsersValue); // �������� �������� ���� "��������"		    				
		    		//����� �������� �� �����������
		    		//AdapterManager.getInstance().getSecuredUDFAdapterManager().setTaskUdfValue(scAdm, UDFReadUsersID , arg0.getTaskId(), readUsersValue);
		   }
		    */	
		   
		   
		   
		  //������ ��������� ������� ��������� �� ����� �������, ��������� � ������ SetReadAllStatus
		   //��� ��� ��� ������� ��������� ������ ���������� ����� ������� ��������� � ��������������
		   	
		}
		catch (Exception ex)
		{
			// ������ ��� ��������� �������� �������� ��������
			DescriptionAccessor.AppendMessageDescription(arg0, "��� ���������� �������� �������� ����������: "+ ex.getMessage()+"\n �������� �������������� TrackStudio ");
			TaskCreator.CreateTaskForAdmin("���������� ��� ���������� �������� ��������� ������������� @"+arg0.getSubmitter().getLogin(), arg0.getTask().getTaskNumber());
		}
		    
		
		return arg0;
	}

}

//TODO Auto-generated method stub
		//������������� ���� ����� ��� ������ ������ ������� ������������, �������� �������������� ����� ���������. ������� ����� ������ ����� ��������� �������

		//SessionContext rootSession = SessionManager.getInstance().getSessionContext(SessionManager.getInstance().create(UserRelatedManager.getInstance().find("1")));
		//String oldValueReadUsers=AdapterManager.getInstance().getSecuredUDFAdapterManager().getTaskUDFValue(rootSession, UDFIDReadUsers, udfReadUsers.getCaption()) ; //setTaskUdfValue(rootSession, UDFIDReadUsers, arg0.getTask().getId(), "value");
		
