package scripts.before_add_message;

import com.trackstudio.exception.GranException;
import com.trackstudio.external.OperationTrigger;
import com.trackstudio.secured.SecuredMessageTriggerBean;

import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.ValueContainer;

public class SetServiceFieldAsLastHandlerUserId implements OperationTrigger {

	@Override
	public SecuredMessageTriggerBean execute(SecuredMessageTriggerBean arg0) throws GranException {
		// TODO Auto-generated method stub
		UDFValueAccessor accessor= new  UDFValueAccessor();
		accessor.SetUDFValueByAdmin(UDF_IDs.ServiceField, arg0.getTask(), arg0.getTask().getHandlerUserId(), false); 
		return arg0;
	}

}
