package scripts.before_add_message;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.OperationTrigger;
import com.trackstudio.kernel.manager.MessageManager;
import com.trackstudio.kernel.manager.SafeString;
import com.trackstudio.kernel.manager.TaskManager;
import com.trackstudio.kernel.manager.UdfManager;
import com.trackstudio.secured.SecuredMessageBean;
import com.trackstudio.secured.SecuredMessageTriggerBean;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredUDFBean;

import scripts.helpers.TaskCreator;

public class AutoBugTest implements OperationTrigger {

	@Override
	
	public SecuredMessageTriggerBean execute(SecuredMessageTriggerBean arg0) throws GranException {
		TaskCreator.CreateTaskForAdmin("������!",arg0.getTask().getNumber());
		return null;
	    	
	}

}
