package scripts.before_add_message;

import com.trackstudio.exception.GranException;
import com.trackstudio.exception.UserException;
import com.trackstudio.external.OperationTrigger;
import com.trackstudio.secured.SecuredMessageTriggerBean;

import scripts.helpers.TaskCreator;
import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.ValueContainer;

public class CheckRequestedSectionPriority implements OperationTrigger {

	@Override
	public SecuredMessageTriggerBean execute(SecuredMessageTriggerBean arg0) throws GranException {
		// TODO Auto-generated method stub
		UDFValueAccessor udfAccessor= new  UDFValueAccessor();
		try
		{
			ValueContainer vc = udfAccessor.GetUDFValue(UDF_IDs.RequestedPriority, arg0, false);
			if (vc.isExist())
			{
				Integer value= Integer.parseInt((String)vc.getValue());
				if (value<=0)
				{
					throw new UserException("�������� ���������� ���������� ������ ���� ������ 0");
				}
				vc = udfAccessor.GetUDFValue("Integer", UDF_IDs.SectionPriority, arg0.getTask(), false);
				
				if (vc.isExist())
				{
					Integer currentValue= (Integer)vc.getValue();
					if (value>=currentValue)
					{
						throw new UserException("�������� ���������� ���������� ������ ���� ������ �������� ��������: "+currentValue);
					}
				}
				
			}
			else
			{
				udfAccessor.SetErrorUDFValue(arg0.getTask(), vc.toString(), false);
			}
		}
		catch (UserException ex)
		{
			throw ex;
		}
		catch (Exception ex)
		{
			TaskCreator.CreateTaskForAdminAboutException("execute", this.getClass().getName(), "���������� � before_add_message trigger", arg0.getTask().getNumber(), ex);
		}
		return arg0;
	}

}
