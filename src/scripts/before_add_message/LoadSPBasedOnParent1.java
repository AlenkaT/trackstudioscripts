package scripts.before_add_message;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Scanner;

import com.trackstudio.app.TriggerManager;
import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.app.session.SessionManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.OperationTrigger;
import com.trackstudio.kernel.manager.MessageManager;
import com.trackstudio.kernel.manager.SafeString;
import com.trackstudio.kernel.manager.TaskManager;
import com.trackstudio.kernel.manager.UdfManager;
import com.trackstudio.kernel.manager.UserManager;
import com.trackstudio.secured.SecuredMessageBean;
import com.trackstudio.secured.SecuredMessageTriggerBean;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredUDFBean;

public class LoadSPBasedOnParent1 implements OperationTrigger {

	@Override
	//��������� ������ ������� �� ���������� �����
	public SecuredMessageTriggerBean execute(SecuredMessageTriggerBean msg) throws GranException {
		
		SecuredTaskBean parent = msg.getTask();
		/*
		String udfGlobalDirId="402881816f228b86016fb354e39f30e7"; //id ���� Global Dir ��� �����			
		SecuredUDFBean udfGlobalDir = AdapterManager.getInstance().getSecuredFindAdapterManager().findUDFById(task.getSecure(), udfGlobalDirId);
		
		String globalDirValue_r=((String)task.getUdfValueByName(udfGlobalDir.getCaption()));
		
		String globalDirValue=globalDirValue_r.replaceAll("Q:","D:\\\\NetworkDrives\\\\Object").replaceAll("T:","D:\\\\NetworkDrives\\\\Mail").replaceAll("S:", "D:\\\\NetworkDrives\\\\Obmen").replaceAll("R:", "D:\\\\NetworkDrives\\\\Arxiv");
		
		
		FileReader fr;
		List<String> arr = new ArrayList<String>();
		try 
		{
			fr = new FileReader(globalDirValue+"\\BuildingHelper.txt" );
			
			Scanner scan = new Scanner(fr); 
	        int i=0;
	        while (scan.hasNextLine()) 
	        {
	        	if (i>0)
	        	{  
	        		boolean answer= arr.add(scan.nextLine());
	        	}
	        	else
	        	{
	        		scan.nextLine();
	        	}
	        	i=i+1;
	        }
	        scan.close();
	        fr.close();
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			msg.setDescription(e.getMessage());
		}
        
		
		*/
        String parentId=null;
        String categoryId="null";
        String udfID1=null;
        String udf1Value="";
        String udfID2=null;
        String udf2Value="";
        String udfID3=null;
        String udf3Value="";
        SafeString shortname=null;
        String name=null;
        String description=null;
        String priorityId="null";
        String handlerId=null;
       
        String userID=null;
        String statusId=null;
        String resolutionId=null;
        Long budget = Long.valueOf(0);
        Calendar deadline=null;
        Calendar submitDate=null;
        Calendar updateDate=null;
        Calendar closeDate=null;
        int number=0;
        
        categoryId="4028808a1951e21b011952bddb38032c";
		//for(String str:arr)
		//{
		try
	    {
			ArrayList<SecuredTaskBean> children= parent.getChildren();
			
			for (SecuredTaskBean task:children ) {
			
			if (!task.getCategoryId().contentEquals(categoryId)) continue;
			
			parentId=parent.getId();
			
			handlerId=task.getHandlerUserId();
			
			statusId="402881816f228b86016f3bac7fa40754";
			priorityId="4028808a1951e21b011952b524ff0288";
			name=task.getName();
			userID=task.getSubmitterId();
			
			//������� ����� ������
			String newtaskID =TriggerManager.getTask().createTask(parentId, userID, categoryId, SafeString.createSafeString(name), deadline, submitDate, updateDate, statusId);
			
			description=task.getDescription();
			
			deadline=task.getDeadline();
			submitDate=task.getSubmitdate();  
			updateDate=task.getUpdatedate();  //10 �� �������
			
			TaskManager.getTask().updateTask(newtaskID , shortname, SafeString.createSafeString(name)
    				,SafeString.createSafeString(description), budget, deadline, priorityId, parentId, handlerId, task.getHandlerGroupId(), 
    				submitDate, updateDate);	

		
			String udfGlobalDirId="402881816f228b86016fb354e39f30e7"; //id ���� Global Dir ��� �����			
			SecuredUDFBean udfGlobalDir = AdapterManager.getInstance().getSecuredFindAdapterManager().findUDFById(task.getSecure(), udfGlobalDirId);
			
			String globalDirValue_r=((String)task.getUdfValueByName(udfGlobalDir.getCaption()));
			AdapterManager.getInstance().getSecuredUDFAdapterManager().setTaskUdfValue(task.getSecure(), udfGlobalDirId , newtaskID, globalDirValue_r);
			
			}
	    }
		
		
		catch (Exception ex)
        {
        	msg.setDescription(ex.getMessage());
        }
      //  } //end for
        

	    return msg;
	    	
	}

}
