package scripts.before_add_message;

import com.trackstudio.exception.GranException;
import com.trackstudio.external.OperationTrigger;
import com.trackstudio.secured.SecuredMessageTriggerBean;

import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.ValueContainer;

public class SetServiceFieldAsOldValueUDFSectionPriority implements OperationTrigger {

	@Override
	public SecuredMessageTriggerBean execute(SecuredMessageTriggerBean arg0) throws GranException {
		// TODO Auto-generated method stub
		UDFValueAccessor accessor= new  UDFValueAccessor();
		ValueContainer vc = accessor.GetUDFValue("Integer", UDF_IDs.SectionPriority, arg0.getTask(), false);
		Integer value;
		if (vc.isExist() && vc.getValue() instanceof Integer)
		{
			value= (Integer)vc.getValue();
		}
		else
		{
			value=0;
		}
		
		accessor.SetUDFValueByAdmin(UDF_IDs.ServiceField, arg0.getTask(), String.valueOf(value), false); // обнуляем требуемый приоритет	
		return arg0;
	}

}
