package scripts.before_edit_task;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.exception.UserException;
import com.trackstudio.external.TaskTrigger;
import com.trackstudio.secured.SecuredTaskTriggerBean;
import com.trackstudio.secured.SecuredUDFBean;
import com.trackstudio.tools.Pair;
import com.trackstudio.tools.textfilter.HTMLEncoder;

import scripts.helpers.MessageGenerator;
import scripts.helpers.RequestGenerator;
import scripts.helpers.TaskCreator;
import scripts.helpers.TaskFieldValuesChecker;
import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UserUDF_IDs;
import scripts.helpers.ValueContainer;

public class CheckGlobalDir implements TaskTrigger {

	@Override
	public SecuredTaskTriggerBean execute(SecuredTaskTriggerBean task) throws GranException
	{
		TaskFieldValuesChecker.CheckFullPathDir(task, "�������������� ������: ");
		
		return task;

	}


}
