package scripts.before_create_task;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.exception.UserException;
import com.trackstudio.external.TaskTrigger;
import com.trackstudio.secured.SecuredTaskTriggerBean;
import com.trackstudio.secured.SecuredUDFBean;
import com.trackstudio.tools.textfilter.HTMLEncoder;

public class CheckGlobalDirAndSelfDir implements TaskTrigger {

	@Override
	public SecuredTaskTriggerBean execute(SecuredTaskTriggerBean task) throws GranException 
		{
		try
		{
			// TODO Auto-generated method stub
			String UDFIDGlobalDir="402881816f228b86016fb354e39f30e7"; //id ���� Global Dir ��� �����
			String UDFIDSelDir="4028818170d27be50171dabe68c538c4";//=������ �����
			// �������� ���� ����� �� ��������������				
			SecuredUDFBean udfGlobalDir = AdapterManager.getInstance().getSecuredFindAdapterManager().findUDFById(task.getSecure(), UDFIDGlobalDir);
			SecuredUDFBean udfSelfDir = AdapterManager.getInstance().getSecuredFindAdapterManager().findUDFById(task.getSecure(), UDFIDSelDir);
			
			//(String)task.getUdfValueByName(udfGlobalDir.getCaption()) - ��� �������� ������ ��� SecuredTaskBean
			
			String globalDirValue=task.getUdfValue(udfGlobalDir.getCaption());
			String selfDirValue=task.getUdfValue(udfSelfDir.getCaption());
					
				if (globalDirValue!=null)
					{
						if (!globalDirValue.isEmpty()) //���� ������ �� ������, �� ������ �� ����������
						{
							
							if (globalDirValue.contains("<����� �������� ������>"))
							{
								String parentGlobalDirValue=(String)task.getParent().getUdfValueByName(udfGlobalDir.getCaption());
								Path parentPath=Paths.get(parentGlobalDirValue.replaceAll("Q:","D:\\\\NetworkDrives\\\\Object").replaceAll("T:","D:\\\\NetworkDrives\\\\Mail")
										.replaceAll("S:", "D:\\\\NetworkDrives\\\\Obmen").replaceAll("R:", "D:\\\\NetworkDrives\\\\Arxiv")
										);
								if (Files.notExists(parentPath) || !Files.isDirectory(parentPath))
								{ 
									throw new UserException("�������, ��������� � GlobalDir ������ ��������� ������, �� ����������");
								}
								
								globalDirValue=parentGlobalDirValue+globalDirValue.replaceAll("<����� �������� ������>", ""); // ��� �������� ������ ���, ���� ������� �� parentGlobalDirValue ����� ������� �����
							}
							task.setUdfValue(udfGlobalDir.getCaption(), globalDirValue);
													
							if (globalDirValue.contains("Q:") || globalDirValue.contains("R:") || globalDirValue.contains("S:") || globalDirValue.contains("T:"))
							{
								//����������� ������ ������� \\\\
	
								String pathStr=globalDirValue.replaceAll("Q:","D:\\\\NetworkDrives\\\\Object").replaceAll("T:","D:\\\\NetworkDrives\\\\Mail")
										.replaceAll("S:", "D:\\\\NetworkDrives\\\\Obmen").replaceAll("R:", "D:\\\\NetworkDrives\\\\Arxiv")
										.replaceAll("\\<������ �����>",""); //��������� �������� ����, ���������� ��� �������� ��� ��������� �������� ������������ �����
								
								Path path= Paths.get(pathStr); //������ ���� � �����, ��� ����� ������ �����
								
								if (Files.notExists(path) || !Files.isDirectory(path))
								{ 
									throw new UserException("�������, ��������� � GlobalDir, �� ����������");
								}
								
								if (globalDirValue.contains("<������ �����>") && selfDirValue!=null && !selfDirValue.isEmpty()) //������� �������� ��� ������ �����
								{
									
									Path newSelfDir=Paths.get(pathStr+"\\"+selfDirValue); 
									if (Files.notExists(newSelfDir))
									{
										try {
											//File file = new File(requestDirName); ������ ����������� �����
											Files.createDirectories(newSelfDir);
											//task.setUdfValue(udfSelfDir.getCaption(), ""); - �� �������� SelfDir
											task.setUdfValue(udfGlobalDir.getCaption(), globalDirValue.replaceAll("<������ �����>",selfDirValue));
										} catch (IOException e) {
											// TODO Auto-generated catch block
											throw new UserException("�� ������� ������� �����" + globalDirValue.replaceAll("<������ �����>",selfDirValue));
										}
									}
									
								}
								
								
							}
							else
							{
								throw new UserException(globalDirValue +"  - ������� �� ��������� �� ����� �� ������ Q, R, S, T");
							}
						
						}

					}
					else
					{
						String curDescription=HTMLEncoder.stripHtmlTags(task.getDescription());
						task.setDescription(curDescription+" �� ������� ��������� ������ �������� ������������� ���������� � GlobalDir �������� (������ 33 ������� null). �������� ������������ �������.");
					}
		}
		catch (NullPointerException ex)
		{
			throw new UserException("��� ���������� ������� CheckGlobalDirAndSelfDir �������� ���������� "+ex.getMessage() + "\n ��������� �������������� ��� ������������ �������.");
			
		}
		catch (InvalidPathException ex)
		{
			throw new UserException("���� Global Dir ��� Self Dir �������� ������������ �������");
			
		}
		
		return task;
	
		}

}
