package scripts.before_create_task;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.exception.UserException;
import com.trackstudio.external.TaskTrigger;
import com.trackstudio.secured.SecuredTaskTriggerBean;
import com.trackstudio.secured.SecuredUDFBean;
import com.trackstudio.tools.textfilter.HTMLEncoder;

import scripts.helpers.MessageGenerator;
import scripts.helpers.StringConstants;
import scripts.helpers.TaskCreator;
import scripts.helpers.TaskFieldValuesChecker;
import scripts.helpers.UDFValueAccessor;
import scripts.helpers.ValueContainer;

public class CheckGlobalDir implements TaskTrigger {

	@Override
	public SecuredTaskTriggerBean execute(SecuredTaskTriggerBean task) throws GranException 
		{
		  try
		  {
			TaskFieldValuesChecker.CheckFullPathDir(task, "�������� ������: ");
		  }
		  catch (UserException ex)
		  {
			  UDFValueAccessor udfAccessor= new UDFValueAccessor();
			  
			  //���� � ����� �� ������
			  ValueContainer vc=udfAccessor.GetAvailableDirPath(task, false); 
			  if (vc.isExist())
			  {
				  String selfDirName=((String)vc.getValue());
				  //TaskCreator.CreateTaskForAdmin("before_create_task.CheckGlobalDir selfDir="+selfDirName, "newTask");			 
				  String dir=TaskFieldValuesChecker.getServerDirPath(selfDirName);
				  	 
				  try {
					 Files.createDirectory(Paths.get(dir)); // ������� ������� �����������
				  	}
				  catch (IOException e) {
					// TODO Auto-generated catch block
					// �������, �� ����������� �������������� UserException
					  throw new UserException("�� ������� ����� ��� ������� �����: "+selfDirName);
				  }
			  }
			  
		  }
		  
		return task;
	}

}
