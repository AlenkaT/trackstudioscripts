package scripts.before_create_task;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;


import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.exception.UserException;
import com.trackstudio.external.TaskTrigger;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredTaskTriggerBean;
import com.trackstudio.secured.SecuredUDFBean;
import com.trackstudio.tools.textfilter.HTMLEncoder;

import scripts.helpers.TaskFieldValuesChecker;
import scripts.helpers.Task_IDs;
import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.ValueContainer;

public class RegisterLetterTest implements TaskTrigger {

	UDFValueAccessor _accessor= new UDFValueAccessor();
	
	@Override
	public SecuredTaskTriggerBean execute(SecuredTaskTriggerBean task) throws GranException
	{
		String curDescription=task.getDescription();
		
		// 1. ��������� ���� � ��������� ������
		
		if (!TaskFieldValuesChecker.CheckFullPathDir(task, "Calling class - RegisterLetter, method - execute")) throw new UserException("�������, ��������� � GlobalDir, �� ����������");
		
		
		//1.5 ��������� ������������ � ������ ��������� ���������
		//�������� ���� "���������� ���������"
		
		ValueContainer subscribedUserListVC=_accessor.GetUDFValue(UDF_IDs.NeedToSend, task);
		if (!subscribedUserListVC.isExist()) 
		{
			throw new UserException("���������� ��������� ������� ����������� ������ ��-�� ���������� ������� � ���� "+subscribedUserListVC.getUDFName()+". �������� �������������� TrackStudio");
		}
		
		String handler="";
	    if (task.getHandler()!=null)
	    {
	    	String subscribedUserList=(String)subscribedUserListVC.getValue();
	    	handler= task.getHandler().getLogin();
	    	if (!subscribedUserList.contains(handler))
	    	{
	    		if (!subscribedUserList.isEmpty())
	    		{
	    			subscribedUserList=subscribedUserList+";";
	    		}
	    		subscribedUserList=subscribedUserList+"@"+handler;
	    	}
	    	task.setUdfValue(subscribedUserListVC.getUDFName(),subscribedUserList);
	    }
			
			
		//2. ������ ��������� ����� ������	
		
		//�������� ���� "����� ��� �����������" 
		ValueContainer vc=_accessor.GetUDFValue(UDF_IDs.RegNumber, task);
		
		if (vc.isExist())
		{			
			String letterType= (String)_accessor.GetUDFValue(UDF_IDs.LetterType, task).getValue();
			String prefix="";
			switch 	(letterType)
			{
				case "��������":
				{
					prefix="��.";
					break;
				}
				case "���������":
				{
					prefix="���.";
					break;
				}
				default: break;
			}	
		
			String regNumber=(String)vc.getValue();
			if (regNumber.isEmpty())
			{
				String code= (String)_accessor.GetUDFValue(UDF_IDs.MdpCode, task).getValue();		
				String postfix="";		
				String udfId="";

				switch 	(letterType)
				{
					case "��������":
					{
						switch 	(code)
						{
							case "MDP":  udfId="402881817424bd210174e8420e4c5b7d"; break;
							case "MDP+": udfId="402881817424bd210174e843928f5bbc"; break;
						}																
						break;
					}
					case "���������":
					{
						switch 	(code)
						{
							case "MDP":  udfId="402881817424bd210174e843c9fb5bd3";	break;
							case "MDP+": udfId="402881817424bd210174e843ca0b5bea"; break;
							
						}
						
						postfix= "-"+String.valueOf(Calendar.getInstance().get(Calendar.YEAR)).substring(2);
						break;
					}
					default: break;

				}
				//path=path+mdpCode+".txt";
				//������ ����� ���������� ���������� ������
				SecuredTaskBean settings=AdapterManager.getInstance().getSecuredFindAdapterManager().findTaskById(task.getSecure(), Task_IDs.LetterSettings);
				Integer number= (Integer)_accessor.GetUDFValue("Integer", udfId, settings, false).getValue()+1;
				task.setUdfValue(vc.getUDFName(), number+postfix);
				prefix=prefix+String.format("%04d", number);

				//�������������� �����
				_accessor.SetUDFValue(udfId, settings, String.valueOf(number), false);
			}
			task.setName(prefix+"   "+task.getName());
			
		}
		else
		{
			task.setDescription(curDescription+"<br>"+"������� ����������� ������ ���������� �����������. �������� �������������� ��. <br>"+vc.getReason());
		}
		return task;

	}
	

}
