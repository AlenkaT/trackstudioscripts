package scripts.task_custom_field_lookup;

import java.util.List;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskUDFLookupScript;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredUDFBean;
import com.trackstudio.tools.textfilter.HTMLEncoder;

import scripts.helpers.StringConstants;
import scripts.helpers.TaskFieldValuesChecker;
import scripts.helpers.UDFValueAccessor;
import scripts.helpers.ValueContainer;

public class GetSelfDirPossibleList implements TaskUDFLookupScript {
	
	private UDFValueAccessor udfAccessor= new  UDFValueAccessor();

	
	private ArrayList<String> getSubDirectoriesForTask(SecuredTaskBean task) throws GranException
	{
		ArrayList<String> list = new ArrayList<String>();
		ValueContainer vc= udfAccessor.GetAvailableDirPath(task, false);
		if (!vc.isExist() || ((String)vc.getValue()).isEmpty()) return null;
		String remouteDir=TaskFieldValuesChecker.getServerDirPath((String)vc.getValue());
		File dir = new File(remouteDir); //path ��������� �� ����������
		File[] arrFiles = dir.listFiles();
		for (File file:arrFiles)
		{
			if (file.isDirectory())
			{
				list.add(file.getName());
			}
		}
		return list;
	}
	
	@Override
	public Object calculate(SecuredTaskBean task) throws GranException {
		// TODO Auto-generated method stub
		if (task==null) return null;
		
		//������ �������������
		if (task.getCategoryForNewTask()==null)
		{
			if (task.getCategoryId().contentEquals("402881816f228b86016fa865c6d31eb1" )) return null; //id ��������� ������
			return getSubDirectoriesForTask(task.getParent());
		}
		
		//������ ��������
		ArrayList<String> list = new ArrayList<String>();
		
		if (task.getCategoryForNewTask().contentEquals("402881816f228b86016fa865c6d31eb1" )) //id ��������� ������
		{
			Calendar calender=Calendar.getInstance();
			String y=String.valueOf(calender.get(Calendar.YEAR));
		
			String strMonth="";
			switch (calender.get(Calendar.MONTH)+1)
			{
			case 1:{strMonth="������"; break; }
			case 2:{strMonth="�������"; break; }
			case 3:{strMonth="����"; break; }
			case 4:{strMonth="������"; break; }
			case 5:{strMonth="���"; break; }
			case 6:{strMonth="����"; break; }
			case 7:{strMonth="����"; break; }
			case 8:{strMonth="������"; break; }
			case 9:{strMonth="��������"; break; }
			case 10:{strMonth="�������"; break; }
			case 11:{strMonth="������"; break; }
			case 12:{strMonth="�������"; break; }
			}
			String remoutePath="D:\\\\NetworkDrives\\\\Mail";
			String path="\\"+y+"\\"+strMonth+"\\";
			LocalDate localDate = LocalDate.now();
					
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.");
			String[] strPath =new String[4];
			strPath[0]=(path+"��������\\"+localDate.format(formatter)+y.substring(2));
			strPath[1]=(path+"���������\\"+localDate.format(formatter)+y.substring(2));
			
			for (String str:strPath)
			{
				String newPath=remoutePath+str;
				if ( Files.exists(Paths.get(newPath)))
				{
					File dir = new File(newPath); //path ��������� �� ����������
					
					Arrays.asList(dir.listFiles()).forEach(i->list.add("T:"+str+"\\"+i.getName()));
				}
			}
			
			return list;
		}
		
		list.add(StringConstants.TaskName);
		list.addAll(getSubDirectoriesForTask(task));
		
		return list;
		
	}

}
