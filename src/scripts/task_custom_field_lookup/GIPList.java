package scripts.task_custom_field_lookup;

import java.util.List;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;

import com.trackstudio.app.Slider;
import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.app.session.SessionContext;
import com.trackstudio.app.session.SessionManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskUDFLookupScript;
import com.trackstudio.kernel.cache.UserRelatedManager;
import com.trackstudio.secured.SecuredMessageBean;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredUDFBean;
import com.trackstudio.secured.SecuredUserBean;
import com.trackstudio.tools.textfilter.HTMLEncoder;

import scripts.helpers.StringConstants;
import scripts.task_custom_field_value.AlreadyReadUsersCalculator;

public class GIPList implements TaskUDFLookupScript {

	
	@Override
	public Object calculate(SecuredTaskBean task) throws GranException {
		ArrayList<String> users= new ArrayList<String>();
		 String filterId="402881817424bd2101742a3667bb05bc"; //������ ����
		 
		 SessionContext sc = SessionManager.getInstance().getSessionContext(SessionManager.getInstance().create(UserRelatedManager.getInstance().find(StringConstants.AdminId)));
		 Slider<SecuredUserBean> slider= AdapterManager.getInstance().getSecuredUserAdapterManager().getUserList(sc, StringConstants.AdminId, filterId, 100, false, null);
		 
		 slider.forEach(user-> {if (!user.getName().contains("���������")) users.add(user.getName());});
		 
		return users;
		
	}
	
	
}
