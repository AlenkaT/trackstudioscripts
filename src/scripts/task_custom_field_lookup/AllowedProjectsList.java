package scripts.task_custom_field_lookup;

import java.util.List;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;

import com.trackstudio.app.Slider;
import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.app.session.SessionContext;
import com.trackstudio.app.session.SessionManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskUDFLookupScript;
import com.trackstudio.kernel.cache.UserRelatedManager;
import com.trackstudio.secured.SecuredMessageBean;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredUDFBean;
import com.trackstudio.secured.SecuredUserBean;
import com.trackstudio.tools.textfilter.HTMLEncoder;

import scripts.helpers.Filter_IDs;
import scripts.helpers.StringConstants;
import scripts.helpers.TaskSearcher;
import scripts.task_custom_field_value.AlreadyReadUsersCalculator;

public class AllowedProjectsList implements TaskUDFLookupScript {

	
	@Override
	public Object calculate(SecuredTaskBean task) throws GranException {
		ArrayList<String> list= new ArrayList<String>();
		Slider<SecuredTaskBean> slider=TaskSearcher.GetTaskListForUser(task.getSecure().getUserId(), Filter_IDs.AllObjects,"1");// �������� ��� ���� �������� ������� ��� �������
		for (SecuredTaskBean t:slider)
		{
			for (SecuredTaskBean child: t.getChildren())
			{
				if (child.getName().contentEquals("!������"))
				{
					list.add(t.getName()+" #"+ t.getNumber());
					break;
				}
			}			
		}
		return list;
		
	}
	
	
}
