package scripts.after_create_task;


import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskTrigger;
import com.trackstudio.secured.SecuredTaskTriggerBean;

import scripts.helpers.TaskEditor;

public class AfterDrawingCreation implements TaskTrigger {
	
	
	@Override
	public SecuredTaskTriggerBean execute(SecuredTaskTriggerBean task) throws GranException
	{
		TaskEditor.ChangeTaskNumbers(task,1, true);
		
		return task;
	}
}


