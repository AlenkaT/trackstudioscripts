package scripts.after_create_task;

import java.util.ArrayList;
import java.util.Comparator;

import com.trackstudio.app.Slider;
import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.app.session.SessionContext;
import com.trackstudio.app.session.SessionManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskTrigger;
import com.trackstudio.kernel.cache.UserRelatedManager;
import com.trackstudio.kernel.manager.SafeString;
import com.trackstudio.kernel.manager.TaskManager;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredTaskTriggerBean;

import scripts.helpers.Filter_IDs;
import scripts.helpers.StringConstants;
import scripts.helpers.TaskCreator;
import scripts.helpers.TaskEditor;
import scripts.helpers.TaskSearcher;
import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.ValueContainer;

public class AfterProjectSectionTest implements TaskTrigger {
	
	
	@Override
	public SecuredTaskTriggerBean execute(SecuredTaskTriggerBean task) throws GranException
	{
		try
		{
		 UDFValueAccessor udfAccessor= new UDFValueAccessor();
		 Slider<SecuredTaskBean> slider=TaskSearcher.GetTaskListForUser(task.getHandlerUserId(), Filter_IDs.MySections,"1");// �������� ��� ���� �������� ������� �������
		 //�������� � �������� ���������� � �������
		 ValueContainer vc;
		 ArrayList<SecuredTaskBean> list =new ArrayList<SecuredTaskBean>();
		 ArrayList<Integer> priorities =new ArrayList<Integer>();
		 Integer countItems=slider.getSize();
		 Integer maxPriority=0;
		 for (SecuredTaskBean item : slider)
		 {
			 vc=udfAccessor.GetUDFValue("Integer",UDF_IDs.SectionPriority , item, false);
			 if (!vc.isExist() || vc.isEmpty()) continue;
			 Integer priority = (Integer)vc.getValue(); // �������� ���� ����������� ��������� ��� ������� ������ �������
			 if (priority>=countItems)
			 {
				 list.add(item);
				 priorities.add(priority);
			 }
			 if (maxPriority<priority) maxPriority=priority;			 
		 }
		 /*
		 Integer index=0;
		 for (SecuredTaskBean item : list)
		 {
			 Integer newPriority=priorities.get(index)-(maxPriority-countItems)-1;
			 udfAccessor.SetUDFValueByAdmin(UDF_IDs.SectionPriority, item, String.valueOf(newPriority), false);
			 index++;
		 }
		 
		 */
		 // �������� �������� ���� ��������� �����������
		 
		//����������� ���������= maxPriority+1
		 udfAccessor.SetUDFValueByAdmin(UDF_IDs.SectionPriority, task, String.valueOf(maxPriority+1), false);
		 
		 
		 
		 //� ������ ���������� ������ � �������� ������, ����� ������ ������ ��������� ������ ������->!!!!!�������� ������ ��� ������� ��� �������� (��� �����)
		 // !!!!!!!!!!!!!!!     �� ��������, ���� ��� ����� ����� ���� 2.1.5
	/*	 String number=getSectionNumber(task); //����� ������� ������
		 int index=number.lastIndexOf("."); 
		 String prefix="";
		 if (index>=0) prefix=number.substring(0, index);
		 for (SecuredTaskBean t: task.getParent().getChildren())
		 {
			 if (t.getNumber()==task.getNumber()) continue;
			 if (t.getName().startsWith(prefix) )
			 {
				 String  t_number=getSectionNumber(t).substring(index+1); // ��������� ������
				 try
				 {
					 if (!t_number.contains(".") && t_number.compareTo(number)>=0) 
					 {
					 	String newNumber=String.valueOf(Integer.valueOf(t_number)+1);
					 	TaskManager.getTask().updateTask(t.getId(), 
						 SafeString.createSafeString(t.getShortname()),SafeString.createSafeString(t.getName().replaceAll(getSectionNumber(t), newNumber)), SafeString.createSafeString(t.getDescription()),
						 t.getBudget(), t.getDeadline(), t.getPriorityId(), t.getParentId(), t.getHandlerUserId(), t.getHandlerGroupId(), t.getSubmitdate(), t.getUpdatedate());
					 }
				 }
				 catch (NumberFormatException ex1)
				 {
					 //������ �������������� �����->������ �� ������
					 continue;
				 }
				 
			 }
		 }
		 
		 */
		 
		 
		 TaskEditor.ChangeTaskNumbers(task, 1, true);
		
		}
		catch (Exception ex)
		{
			TaskCreator.CreateTaskForAdminAboutException("execute", this.getClass().getName(), "",  task.getNumber(), ex);
		}
		return task;
		
	}
}


