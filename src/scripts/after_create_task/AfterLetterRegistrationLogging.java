package scripts.after_create_task;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;


import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.exception.UserException;
import com.trackstudio.external.TaskTrigger;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredTaskTriggerBean;
import com.trackstudio.secured.SecuredUDFBean;
import com.trackstudio.secured.SecuredUDFValueBean;
import com.trackstudio.tools.textfilter.HTMLEncoder;

import scripts.helpers.MessageGenerator;
import scripts.helpers.TaskCreator;
import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.ValueContainer;

public class AfterLetterRegistrationLogging implements TaskTrigger {

	@Override
	public SecuredTaskTriggerBean execute(SecuredTaskTriggerBean task) throws GranException
	{	
		//����������� � MoveToAnotherProject
		try
		{
			UDFValueAccessor accessor= new  UDFValueAccessor();
			ValueContainer vc = accessor.GetUDFValue(UDF_IDs.NewHeadProjectForLetter, task);
			if (vc.isExist() )
			{
				String str=(String)vc.getValue();
				if (!str.isEmpty())
				{
					SecuredTaskBean projectTask=AdapterManager.getInstance().getSecuredTaskAdapterManager().findTaskByNumber(task.getSecure(), str.substring(str.lastIndexOf("#")+1));
					if (projectTask!=null)
					{
						for (SecuredTaskBean t:projectTask.getChildren())
						{
							if (t.getName().contentEquals(("!������")))
							{/*Parameters:
sc - ������ ������������
parentId - ID ������, ���� ���������
taskIds - ID ����������� �����
operation - ��� ���������, COPY ��� CUT
*/
								//String oldParent=arg0.getTask().getParent().getNumber();
								AdapterManager.getInstance().getSecuredTaskAdapterManager().pasteTasks(task.getSecure(), t.getId(), task.getTask().getId(), "CUT");
								//MessageManager.getMessage().updateDescription(arg0.getId(), "was moved from #"+oldParent);
								break;
							}
						}	
					}
					else
					{
						accessor.SetErrorUDFValue(task, "������ "+str.substring(str.lastIndexOf("#"))+" ��� ����������� � ���� ������ �� ������" , false);
						//accessor.SetErrorUDFValue(arg0.getTask(), str.substring(str.lastIndexOf("#")) , false);
					}
				}
				
			}
		
			//������ � ������
			ArrayList<SecuredUDFValueBean> list=AdapterManager.getInstance().getSecuredUDFAdapterManager().getUdfValues(task.getSecure(), task.getId());
			StringBuilder message= new StringBuilder("����������� ������: "+task.getName()+" #"+task.getNumber()+"  ��������� �������<br>");	
			message.append("�����������: "+task.getSubmitter().getName()+"<br>");
			message.append("������������� ��� �����������: "+task.getHandler().getName()+"<br>");
				for (SecuredUDFValueBean bean : list)
				{	
					message.append(bean.getCaption()+": "+bean.getValue()+";<br>");
				}
				
				MessageGenerator.CreateCommentMsg(AdapterManager.getInstance().getSecuredFindAdapterManager().findTaskById(task.getSecure(), "402881817424bd210174e84154ee5b7b"), message.toString(), false);
				accessor.SetUDFValueByAdmin(UDF_IDs.NewHeadProjectForLetter, task, null, false);
		}
		
		catch (Exception ex)
		{
			TaskCreator.CreateTaskForAdminAboutException("execute", this.getClass().getName(), "",  task.getNumber(), ex);
		}
		
		return task;

	}
	

}
