package scripts.task_custom_field_value;

import java.nio.file.Files;
import java.nio.file.Paths;

import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskUDFValueScript;

import com.trackstudio.secured.SecuredTaskBean;


import scripts.helpers.StringConstants;

import scripts.helpers.TaskFieldValuesChecker;
import scripts.helpers.UDFValueAccessor;

import scripts.helpers.ValueContainer;

public class RemoutePathExistenceCalculator implements TaskUDFValueScript {
	private static UDFValueAccessor _udfAccessor= new UDFValueAccessor();
	

	@Override
	public Object calculate(SecuredTaskBean task) throws GranException {
		// TODO Auto-generated method stub
		//��������� ���� � ����������� �� ��������� �������
		try {
		ValueContainer vc= _udfAccessor.GetAvailableDirPath(task, false);
		if (vc.isExist() && !((String)vc.getValue()).isEmpty())
		{
			String dir= TaskFieldValuesChecker.getServerDirPath((String)vc.getValue());
			if (Files.notExists(Paths.get(dir)))
			{
				return StringConstants.PathNotFound;
			}
		}
		}
		catch (Exception ex)
		{
			return StringConstants.IllegalPath;
		}
		return null;
		
	}

}
