package scripts.task_custom_field_value;

import java.util.ArrayList;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskUDFValueScript;
import com.trackstudio.secured.SecuredMessageBean;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredUDFBean;

public class WrittenUsersCalculator implements TaskUDFValueScript {

	private static final String _writeGIPMstatusIdGIP=	"4028818172ef354701739667c3a82f9c";
	private static final String _writeKuratorMstatusId=	"4028818172927f3e0172dc1ad58a1620";
	private static final String _writeDepHeadMstatusId=	"4028818172ef35470173966a7cc4304e";
	
	private static ArrayList<String> getWrittenUsers(SecuredTaskBean task) throws GranException
	{
		ArrayList<SecuredMessageBean> messages= task.getMessages();
		if (messages==null) return null;
		ArrayList<String> users= new ArrayList<String>();
		String login;
		for (SecuredMessageBean message:messages)
		{
			if (message.getMstatusId().contentEquals(_writeGIPMstatusIdGIP) ||message.getMstatusId().contentEquals(_writeKuratorMstatusId)||message.getMstatusId().contentEquals(_writeDepHeadMstatusId))
			{
				if (message.getResolution().getName().contentEquals("���������"))
				{
					login=message.getSubmitter().getLogin();
					if (users.contains(login)) continue;
					users.add(login);
				}
			}
		}
		return users;
		
	}
	
	
	@Override
	public Object calculate(SecuredTaskBean task) throws GranException {
		// TODO Auto-generated method stub
		return WrittenUsersCalculator.getWrittenUsers(task);
	}

}
