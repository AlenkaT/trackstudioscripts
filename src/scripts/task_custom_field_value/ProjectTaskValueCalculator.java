package scripts.task_custom_field_value;


import java.util.ArrayList;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.app.session.SessionContext;

import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskUDFValueScript;

import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredUDFBean;

import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.ValueContainer;
import scripts.helpers.WF_IDs;


public class ProjectTaskValueCalculator implements TaskUDFValueScript {
	
	SecuredUDFBean ptUdf;
	SessionContext scRoot;

 private String GetProjectTasNumber(SecuredTaskBean child) throws GranException
	{
		SecuredTaskBean task=child.getParent();
		if (task==null) return "";
		//���� �� ������ ��� ������ ������ �������� ���� ���� ������
		
		
		if (!task.getWorkflowId().contentEquals(WF_IDs.ProjectWorking))
		{
			return GetProjectTasNumber(task);
		}
		else
		{
			return task.getNumber();
		}
			
	}


	@Override
	public Object calculate(SecuredTaskBean task) throws GranException {
		// TODO Auto-generated method stub
		//String drawingWorkflowId="4028808a1951e21b011952b524ff0286";
		ptUdf = AdapterManager.getInstance().getSecuredFindAdapterManager().findUDFById(task.getSecure(), UDF_IDs.ProjectTask);	
		UDFValueAccessor accessor=new UDFValueAccessor();
		//ValueContainer vc= accessor.
		if (ptUdf==null) return null;
		if (task.getWorkflowId().contains((WF_IDs.ProjectWorking))) return null;
		
		String taskNumber= GetProjectTasNumber(task);
		ArrayList<String> result= new ArrayList<String>();
		result.add(taskNumber);
		return result;
	}

}
