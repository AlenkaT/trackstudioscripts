package scripts.task_custom_field_value;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.app.session.SessionContext;
import com.trackstudio.app.session.SessionManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskUDFValueScript;
import com.trackstudio.kernel.cache.UserRelatedManager;
import com.trackstudio.secured.SecuredMessageBean;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredUDFBean;
import com.trackstudio.secured.SecuredUserBean;
import com.trackstudio.tools.Pair;

import scripts.helpers.StringConstants;
import scripts.helpers.TaskFieldValuesChecker;
import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.ValueContainer;

public class FileNameCalculator implements TaskUDFValueScript {
	static UDFValueAccessor accessor= new UDFValueAccessor();
	private static boolean IsPossibleName(String filename)
	{
		return !filename.endsWith(".bak") && !filename.endsWith(".dwl") && !filename.endsWith(".dwl2") && !filename.endsWith(".db") && !filename.endsWith(".err") 
				&& !filename.endsWith(".plot") &&  !filename.endsWith(".zip") && !filename.endsWith(".rar");
	}

	public static ArrayList<File> getFilesForTask( SecuredTaskBean task)
	{
		ArrayList<File> files=new ArrayList<File>();
		try
		{
		ValueContainer vc= accessor.GetAvailableDirPath(task, false);
		if (!vc.isExist() || ((String)vc.getValue()).isEmpty()) return files;
		String name=task.getName();
		String[] lines=name.split("_");
		String number=lines[0];
		//return (String)vc.getValue();
		
		
		String remouteDir=TaskFieldValuesChecker.getServerDirPath((String)vc.getValue());
		File dir = new File(remouteDir); //path ��������� �� ����������
		
		File[] arrFiles = dir.listFiles();
		
		vc=accessor.GetUDFValue("Pair", UDF_IDs.TemplatePath, task,false);
		
		if (vc.isExist() && (vc.getValue() instanceof Pair) && ((com.trackstudio.tools.Pair)vc.getValue()).getKey().contentEquals(UDF_IDs.ParentTemplateDirPathKey) )
		{
			for (File file:arrFiles)
			{
				if (file.isFile() && file.getName().startsWith(number) && IsPossibleName(file.getName()))
				{
					files.add( file);
				}
			}
		}
		else
		{
			for (File file:arrFiles)
			{
				if (file.isFile() && IsPossibleName(file.getName()))
				{
					files.add( file);
				}
			}
		}
		}
		catch (Exception ex)
		{
			//������ �� ������, ������ �� ���������, ���������� , ��� ����
		}
		return files;
		
	}
	@Override
	public Object calculate(SecuredTaskBean task) throws GranException {
		// TODO Auto-generated method stub
		
		
		ArrayList<File> files=getFilesForTask( task);
		
		ArrayList<String> names= new ArrayList<String>(); 
		files.forEach(file->names.add(file.getName()));	
			
		return names;

	}
	
	

}
