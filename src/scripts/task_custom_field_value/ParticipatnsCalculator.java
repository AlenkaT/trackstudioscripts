package scripts.task_custom_field_value;

import java.util.ArrayList;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskUDFValueScript;
import com.trackstudio.secured.SecuredMessageBean;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredUDFBean;

import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.ValueContainer;

public class ParticipatnsCalculator implements TaskUDFValueScript {

	
	private static ArrayList<String> geParticipatnsUsers(SecuredTaskBean task) throws GranException
	{
		
		UDFValueAccessor udfAccessor= new  UDFValueAccessor();
		ValueContainer vc= udfAccessor.GetUDFValueByAdmin( UDF_IDs.ServiceField, task, false);
		if (vc.isExist() && ((String)vc.getValue()).contentEquals("RemoveParticipatns")) return null;
		udfAccessor.SetErrorUDFValue(task,(String)vc.getValue() ,false);
		ArrayList<SecuredMessageBean> messages= task.getMessages();
		if (messages==null) return null;
		ArrayList<String> users= new ArrayList<String>();
		
		
		String login;
		for (SecuredMessageBean message:messages)
		{
				login=message.getSubmitter().getLogin();
				if (users.contains(login)) continue;
				users.add(login);	
		}
		return users;
	}
	
	
	@Override
	public Object calculate(SecuredTaskBean task) throws GranException {
		// TODO Auto-generated method stub
		return geParticipatnsUsers(task);
	}
	
	public static ArrayList<String> GetAlreadyReadUsers(SecuredTaskBean task) throws GranException 
	{
		// TODO Auto-generated method stub
		return geParticipatnsUsers(task);
	}

}
