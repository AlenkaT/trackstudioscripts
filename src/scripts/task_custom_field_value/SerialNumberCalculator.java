package scripts.task_custom_field_value;

import java.util.ArrayList;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.app.session.SessionContext;
import com.trackstudio.app.session.SessionManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskUDFValueScript;
import com.trackstudio.kernel.cache.UserRelatedManager;
import com.trackstudio.secured.SecuredMessageBean;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredUDFBean;
import com.trackstudio.secured.SecuredUserBean;

import scripts.helpers.StringConstants;
import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.ValueContainer;

public class SerialNumberCalculator implements TaskUDFValueScript {


	@Override
	public Object calculate(SecuredTaskBean task) throws GranException {
		// TODO Auto-generated method stub
		String name=task.getName();
		String[] lines=name.split("_");
		String[] nums=lines[0].split("\\.");
		try
		{
			return Integer.parseInt(nums[0]);
		}
		catch (NumberFormatException e)
		{
			return null;
		}

	}
	
	

}
