package scripts.task_custom_field_value;

import java.util.ArrayList;
import java.util.Calendar;

import org.jivesoftware.smackx.packet.VCard;

import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskUDFValueScript;
import com.trackstudio.secured.SecuredMessageBean;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredUserBean;

import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.ValueContainer;


public class SubscribersListCalculator implements TaskUDFValueScript {

	private static final String _subscribeMstatusId=	"402881817424bd2101746e4ece463572";
	//private static final String _subscribeForMstatusId=	"402881817424bd21017503767fa0729b";
	
	@Override
	public Object calculate(SecuredTaskBean task) throws GranException {
		// TODO Auto-generated method stub
		ArrayList<SecuredMessageBean> messages= task.getMessages();
		ArrayList<String> users= new ArrayList<String>();
		if (messages!=null) 
		{
		String login;
		for (SecuredMessageBean message:messages)
		{
			if (message.getMstatusId().contentEquals(_subscribeMstatusId) )
			{
				login=message.getSubmitter().getLogin();
				if (users.contains(login)) continue;
				users.add(login);	
				continue;
			}	
		}
		}
		
		UDFValueAccessor accessor= new UDFValueAccessor();
		accessor.SetErrorUDFValue(task, "", false);
		
				
		//ValueContainer result = accessor.GetUDFValue("ArrayList<SecuredUserBean>", UDF_IDs.SubscribedUsers,task, false); //�������� ���� �������� �������� ���
		ValueContainer result =accessor.GetUDFValueByAdmin(UDF_IDs.SubscribedUsers, task, false);
		if (result.isExist() && (String)result.getValue()!="")
		{
			accessor.SetErrorUDFValue(task, (String)result.getValue(), false);
			String[] list=((String)result.getValue()).split(";");
			//ArrayList<SecuredUserBean> list = (ArrayList<SecuredUserBean>)result.getValue();
			
			for (String user: list )
			{
				if (users.contains(user.substring(1))) continue;
				accessor.AddErrorUDFValue(task, user, false);
				users.add(user.substring(1));	
			}
			
			
		}
		
		
	return users;
	}
	


}
