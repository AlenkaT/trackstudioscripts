package scripts.task_custom_field_value;

import java.util.HashMap;
import java.util.Map.Entry;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskUDFValueScript;
import com.trackstudio.kernel.manager.UdfManager;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredTaskTriggerBean;
import com.trackstudio.secured.SecuredUDFBean;
import com.trackstudio.tools.Pair;

import scripts.helpers.StringConstants;
import scripts.helpers.TaskCreator;
import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.ValueContainer;

public class RemouteDirPathCalculator implements TaskUDFValueScript {
	private static UDFValueAccessor _udfAccessor= new UDFValueAccessor();
	
	private static String getFullDirPath(SecuredTaskBean task) throws GranException
	{
		
		
		ValueContainer vc=_udfAccessor.GetUDFValue("Pair", UDF_IDs.TemplatePath, task,false);
		
		if (vc.isExist() && (vc.getValue() instanceof Pair) )
		{
			
				com.trackstudio.tools.Pair pair=(com.trackstudio.tools.Pair)vc.getValue();
				String key = pair.getKey();
			
			
			String parantDirValue="";
			switch (key)
			{
				//������������ �����
				case UDF_IDs.ParentTemplateDirPathKey: 
					vc=_udfAccessor.GetAvailableDirPath(task.getParent(), true);
					//vc= udfAccessor.GetUDFValue(String.class, UDF_IDs.SelfDir, task.getParent(), false);
				break;
				// ��������+���
				case UDF_IDs.ParentPlusSelfTemplateDirPathKey :
					vc= _udfAccessor.GetAvailableDirPath(task.getParent(),true);
					//vc= udfAccessor.GetUDFValue(String.class, UDF_IDs.SelfDir, task.getParent(), false);
					if (vc.isExist())
					{
						parantDirValue=(String)vc.getValue();
						vc= _udfAccessor.GetUDFValue("String", UDF_IDs.SelfDir, task, false);
						
					}
				break;
				default:vc= _udfAccessor.GetUDFValue("String", UDF_IDs.SelfDir, task, false);
				
			}
			if (vc.isExist())
			{
				String selfDir=((String)vc.getValue()).replaceAll(StringConstants.TaskName, task.getName());
				if (!parantDirValue.isEmpty() && !parantDirValue.endsWith("\\")) return parantDirValue+"\\"+selfDir;
				return parantDirValue+selfDir;
			}
		}
			
		return null;
	}
	
	
	private static String getFullDirPath(SecuredTaskTriggerBean task) throws GranException
	{
		UDFValueAccessor udfAccessor= new UDFValueAccessor();
		SecuredUDFBean udfBean = AdapterManager.getInstance().getSecuredFindAdapterManager().findUDFById(task.getSecure(), UDF_IDs.TemplatePath);
		if (udfBean==null) 
		{
			//��� ������� � ����
			return null;
		}
		HashMap<String, String> list=udfBean.getUL();

		ValueContainer vc=udfAccessor.GetUDFValue(UDF_IDs.TemplatePath, task);
		
		if (vc.isExist())
		{
			String value=(String)vc.getValue();
			String parantDirValue="";

			for (Entry<String,String> entry: list.entrySet())	
			{
				
				if (entry.getValue().contentEquals(value))
				{
					switch (entry.getKey())
					{
					//������������ �����
						case UDF_IDs.ParentTemplateDirPathKey: 
							vc=_udfAccessor.GetAvailableDirPath(task.getParent(), true);
							
							break;
							// ��������+���
						case UDF_IDs.ParentPlusSelfTemplateDirPathKey :
							vc= _udfAccessor.GetAvailableDirPath(task.getParent(),true);
							
							if (vc.isExist())
							{
								parantDirValue=(String)vc.getValue();
								vc= _udfAccessor.GetUDFValue(UDF_IDs.SelfDir, task);
						
							}
							break;
						default:vc=_udfAccessor.GetUDFValue(UDF_IDs.SelfDir, task);
					}
					if (vc.isExist())
					{
						String selfDir=((String)vc.getValue()).replaceAll(StringConstants.TaskName, task.getName());
						if (!parantDirValue.isEmpty() && !parantDirValue.endsWith("\\")) return parantDirValue+"\\"+selfDir;
						return parantDirValue+selfDir;
					}
				}
			}
			
		}
		return null;
	}
	
	
	public static String GetFullDirPath(SecuredTaskBean task) throws GranException
	{
		if (task instanceof SecuredTaskTriggerBean)
		{
			
			return getFullDirPath((SecuredTaskTriggerBean)task);
		}
		else
		{
			//TaskCreator.CreateTaskForAdmin("getFullDirPath(task)", task.getTaskNumber());
			return getFullDirPath(task);
		}
	}
	
	
	@Override
	public Object calculate(SecuredTaskBean task) throws GranException {
		// TODO Auto-generated method stub
		//��������� ���� � ����������� �� ��������� �������
		
		return getFullDirPath(task);
		//return "ytn";	
	}

}
