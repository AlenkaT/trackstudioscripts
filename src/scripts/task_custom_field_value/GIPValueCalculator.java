package scripts.task_custom_field_value;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Calendar;

import com.trackstudio.app.Slider;
import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.app.session.SessionContext;

import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskUDFValueScript;
import com.trackstudio.secured.SecuredMessageBean;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredUDFBean;
import com.trackstudio.secured.SecuredUserBean;

import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.WF_IDs;


public class GIPValueCalculator implements TaskUDFValueScript {

	SecuredUDFBean gipProjectUdf;
	SessionContext scRoot;

/*
	private String GetParentGIPUDFValue(SecuredTaskBean child) throws GranException
	{
		SecuredTaskBean task=child.getParent();
		if (task==null) return "";
		//���� �� ������ ��� ������ ������ �������� ���� ���� ������
	/*	Object value=task.getUdfValueByName(gipProjectUdf.getCaption());
		//String value=AdapterManager.getInstance().getSecuredUDFAdapterManager().getTaskUDFValue(task.getSecure(), gipProjectUdfId, task.getId());
		if (value==null || !(value instanceof com.trackstudio.tools.Pair))
		{
			return GetParentGIPUDFValue(task);
		}
		else
		{
			ArrayList<SecuredUserBean> user=null;
			String userName = ((com.trackstudio.tools.Pair)value).getValue();
			return userName;
		}
		
	
		
	}
	
*/
	public static String GetProjectHandler(SecuredTaskBean child) throws GranException
	{
		SecuredTaskBean task=child.getParent();
		if (task==null) return "";
		//���� �� ������ ��� ������ ������ �������� ���� ���� ������
		
		
		if (!task.getWorkflowId().contentEquals(WF_IDs.ProjectWorking))
		{
			return GetProjectHandler(task);
		}
		else
		{
			if (task.getHandlerUser()!=null)
			{
				return task.getHandlerUser().getLogin();
			}
			else
				return "";
			
		}
			
	}

	/* ���� �� ������ ��� ������ ����� ����������
	private ArrayList<String> GetUserByName(ArrayDeque<String> deque, String name) throws GranException
	{
		String filterId="4028818172ef354701732321d7321c70";
		Slider<SecuredUserBean> slider=AdapterManager.getInstance().getSecuredUserAdapterManager().getUserList(scRoot, "1",filterId , 1000, false, null);
		ArrayList<String> res=new ArrayList<String>();
		slider.forEach((user)->res.add(user.getLogin()));
		//withUdf - ����� �� ����������� ���������������� ����
		//order - ������� ����������
		//if (deque.peekFirst()==null) return null;
		//ArrayList<SecuredUserBean> list=AdapterManager.getInstance().getSecuredUserAdapterManager().getChildren(scRoot, deque.pollFirst());
		
		//res.add(scRoot.getUser());
		return res
	}
	*/
	private String getResendOperationSubmitter(SecuredTaskBean child) throws GranException
	{
		SecuredTaskBean task=child.getParent();
		if (task==null) return null;
		
		if (!task.getWorkflowId().contentEquals(WF_IDs.SectionWorking))
		{
			return getResendOperationSubmitter(task);
		}
		else
		{
			String resendMstatusId="4028818172e0c3d40172e1d2838709bf"; //id �������� �������������
			String prstatusId="402881816f228b86016f2329653d00ca";
			ArrayList<SecuredMessageBean> messages= task.getMessages();
			if (messages==null) return null;
			String login=null;
			Calendar date = task.getSubmitdate();
			
			for (SecuredMessageBean message:messages)
			{
				if (message.getMstatusId().contentEquals(resendMstatusId) && //SecuredUserBean.getPrstatusForTaskId(task.getSecure(),message.getSubmitter().getId() ).values().contains(prstatusId) &&
					message.getTime().compareTo(date)>0 )
				{
					date=message.getTime();
					login=message.getSubmitter().getLogin();
					
				}
			}
			return login;
		}
	}

	@Override
	public Object calculate(SecuredTaskBean task) throws GranException {
		// TODO Auto-generated method stub
		//String drawingWorkflowId="4028808a1951e21b011952b524ff0286";
		//mstatusId=4028818172e0c3d40172e1d2838709bf //id �������� �������������
		ArrayList<String> result= new ArrayList<String>();
		
		gipProjectUdf = AdapterManager.getInstance().getSecuredFindAdapterManager().findUDFById(task.getSecure(), UDF_IDs.ProjectGip);	
		
		if (gipProjectUdf==null) return null;
		
		String resenderGip=getResendOperationSubmitter(task);
		
		if (resenderGip!=null ) 
		{
			result.add(resenderGip);
		}
		else
		{
			String userLogin=GetProjectHandler(task);
			result.add(userLogin);
		}
		
		return result;
	}

}
