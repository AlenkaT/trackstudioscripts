package scripts.task_custom_field_value;

import java.util.ArrayList;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.app.session.SessionContext;
import com.trackstudio.app.session.SessionManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskUDFValueScript;
import com.trackstudio.kernel.cache.UserRelatedManager;
import com.trackstudio.secured.SecuredMessageBean;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredUDFBean;
import com.trackstudio.secured.SecuredUserBean;

import scripts.helpers.StringConstants;
import scripts.helpers.TaskCreator;
import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.ValueContainer;

public class CoordinatorCalculator implements TaskUDFValueScript {


	@Override
	public Object calculate(SecuredTaskBean task) throws GranException {
		// TODO Auto-generated method stub
		ArrayList<String> users=new ArrayList<String>();
		UDFValueAccessor udfAccessor= new  UDFValueAccessor();
		ValueContainer vc= udfAccessor.GetUDFValueByAdmin( UDF_IDs.CoordinatorsList, task, false);
		
		if (vc.isExist())
		{
			String name=(String)vc.getValue();
			SessionContext sc = SessionManager.getInstance().getSessionContext(SessionManager.getInstance().create(UserRelatedManager.getInstance().find(StringConstants.AdminId)));	 
			SecuredUserBean user=AdapterManager.getInstance().getSecuredUserAdapterManager().findByName(sc, name);
			if (user!=null)
			{
				users.add(user.getLogin());
				return users;
			}
			
		}
		else
		{
			TaskCreator.CreateTaskForAdmin("������� ���� �� ���� ��������: "+vc.toString(),task.getTaskNumber());
		}
		
		return users;
	}
	
	

}
