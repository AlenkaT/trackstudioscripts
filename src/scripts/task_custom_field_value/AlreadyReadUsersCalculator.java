package scripts.task_custom_field_value;

import java.util.ArrayList;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskUDFValueScript;
import com.trackstudio.secured.SecuredMessageBean;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredUDFBean;

public class AlreadyReadUsersCalculator implements TaskUDFValueScript {

	private static final String _readMstatusId=	"402881816f228b86016fa86ec6721f61";
	private static final String _resendMstatusId=	"402881816f228b86016fa86dd9931f3c";
	
	private static ArrayList<String> getAlreadyReadUsers(SecuredTaskBean task) throws GranException
	{
		ArrayList<SecuredMessageBean> messages= task.getMessages();
		if (messages==null) return null;
		ArrayList<String> users= new ArrayList<String>();
		String login;
		for (SecuredMessageBean message:messages)
		{
			if (message.getMstatusId().contentEquals(_readMstatusId) ||message.getMstatusId().contentEquals(_resendMstatusId))
			{
				login=message.getSubmitter().getLogin();
				if (users.contains(login)) continue;
				users.add(login);	
			}
		}
		return users;
	}
	
	
	@Override
	public Object calculate(SecuredTaskBean task) throws GranException {
		// TODO Auto-generated method stub
		return AlreadyReadUsersCalculator.getAlreadyReadUsers(task);
	}
	
	public static ArrayList<String> GetAlreadyReadUsers(SecuredTaskBean task) throws GranException 
	{
		// TODO Auto-generated method stub
		return getAlreadyReadUsers(task);
	}

}
