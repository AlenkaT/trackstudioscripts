package scripts.task_custom_field_value;


import java.util.ArrayList;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.app.session.SessionContext;

import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskUDFValueScript;

import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.secured.SecuredUDFBean;

import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.ValueContainer;
import scripts.helpers.WF_IDs;


public class SectionCodeCalculator implements TaskUDFValueScript {
	
	SecuredUDFBean ptUdf;
	SessionContext scRoot;
	static UDFValueAccessor accessor=new UDFValueAccessor();

 public static String GetProjectCode(SecuredTaskBean child) throws GranException
	{
		SecuredTaskBean task=child.getParent();
		if (task==null) return "";
		//���� �� ������ ��� ������ ������ �������� ���� ���� ������
		
		
		if (!task.getWorkflowId().contentEquals(WF_IDs.ProjectWorking))
		{
			return GetProjectCode(task);
		}
		else
		{
			ValueContainer vc = accessor.GetUDFValue("String", UDF_IDs.ProjectCode, task, false);
			if (!vc.isExist()) return "";
			return (String)vc.getValue();
		}
			
	}


	@Override
	public Object calculate(SecuredTaskBean task) throws GranException {
		// TODO Auto-generated method stub

		ValueContainer vc = accessor.GetUDFValue("String", UDF_IDs.ShortSectionCode, task, false);
		
		if (vc.isExist())
		{
			return GetProjectCode(task)+"-"+ (String)vc.getValue();
		}
		
		return GetProjectCode(task)+"-";
	}

}
