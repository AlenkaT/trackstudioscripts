package scripts.task_custom_field_value;

import java.util.ArrayList;
import java.util.Calendar;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskUDFValueScript;
import com.trackstudio.secured.SecuredMessageBean;
import com.trackstudio.secured.SecuredTaskBean;

import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;


public class NotificationWasntGotListUsersCalculator implements TaskUDFValueScript {

	
	private static final String _getUpdateMstatusId=    "402881817424bd21017471a13e4436d0";
	
	@Override
	public Object calculate(SecuredTaskBean task) throws GranException {
		// TODO Auto-generated method stub
		ArrayList<SecuredMessageBean> messages= task.getMessages();
		if (messages==null) return null;
		ArrayList<String> users= (ArrayList<String>) new SubscribersListCalculator().calculate(task); //������ ����������� �������������
		UDFValueAccessor accessor= new UDFValueAccessor();
		
		if (users.size()>0)
		{
		Object obj=new DrawingUpdateDateCalculator().calculate(task);
		if (obj==null) return users;
		
		Calendar date= (Calendar) new DrawingUpdateDateCalculator().calculate(task); //���� ���������� ����������
		
		accessor.SetErrorUDFValue(task, "����", false);
		String login;
		for (SecuredMessageBean message:messages)
		{
			//accessor.AddErrorUDFValue(task, "message.getSubmitter().getLogin()", false);
			if (message.getMstatusId().contentEquals(_getUpdateMstatusId) && message.getTime().compareTo(date)>0 )
			{
				
				login=message.getSubmitter().getLogin();
				if (users.contains(login))
					{
					 	users.remove(login);
					 	accessor.AddErrorUDFValue(task, "user: "+login +" was removed", false);
					}
			}
		}
		
		for (String user:users)
		{
			accessor.AddErrorUDFValue(task, user, false);
		}
				
		}
		//AdapterManager.getInstance().getSecuredUDFAdapterManager().setTaskUdfValue(task.getSecure(), UDF_IDs.NotificationWasntGotUsers, task.getId(), value);
		accessor.SetErrorUDFValue(task, "", false);
		return users;
	}
	


}
