package scripts.task_custom_field_value;

import java.util.ArrayList;
import java.util.Calendar;


import com.trackstudio.exception.GranException;
import com.trackstudio.external.TaskUDFValueScript;
import com.trackstudio.secured.SecuredMessageBean;
import com.trackstudio.secured.SecuredTaskBean;


public class DrawingUpdateDateCalculator implements TaskUDFValueScript {

	private static final String _updateMstatusId=	"402881817424bd2101746e3bb71f3457";
	

	@Override
	public Object calculate(SecuredTaskBean task) throws GranException {
		// TODO Auto-generated method stub
		ArrayList<SecuredMessageBean> messages= task.getMessages();
		if (messages==null) return null;
		Calendar date= task.getSubmitdate();
		for (SecuredMessageBean message:messages)
		{
			if (message.getMstatusId().contentEquals(_updateMstatusId) && date.compareTo(message.getTime())<0)
			{
				date= message.getTime();
			}
		}
		//if (date.compareTo( task.getSubmitdate())>0)
		return date; 
		//return null;
		
		
	}
	


}
