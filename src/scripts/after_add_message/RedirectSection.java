package scripts.after_add_message;

import java.util.ArrayList;

import com.trackstudio.app.Slider;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.OperationTrigger;
import com.trackstudio.secured.SecuredMessageTriggerBean;
import com.trackstudio.secured.SecuredTaskBean;

import scripts.helpers.Filter_IDs;
import scripts.helpers.TaskCreator;
import scripts.helpers.TaskSearcher;
import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.ValueContainer;

public class RedirectSection implements OperationTrigger {

	@Override
	public SecuredMessageTriggerBean execute(SecuredMessageTriggerBean arg0) throws GranException {
		// TODO Auto-generated method stub
		try
		{
		 UDFValueAccessor udfAccessor= new UDFValueAccessor();
		 Slider<SecuredTaskBean> slider=TaskSearcher.GetTaskListForUser(arg0.getHandlerUserId(), Filter_IDs.MySections,"1");// �������� ��� ���� �������� ������� �������
		 //�������� � �������� ���������� � �������
		 ValueContainer vc;
		 ArrayList<SecuredTaskBean> list =new ArrayList<SecuredTaskBean>();
		 ArrayList<Integer> priorities =new ArrayList<Integer>();
		 Integer countItems=slider.getSize();
		 Integer maxPriority=0;
		 for (SecuredTaskBean item : slider)
		 {
			 vc=udfAccessor.GetUDFValue("Integer",UDF_IDs.SectionPriority , item, false);
			 if (!vc.isExist() || vc.isEmpty()) continue;
			 Integer priority = (Integer)vc.getValue();
			 if (priority>=countItems)
			 {
				 list.add(item);
				 priorities.add(priority);
			 }
			 if (maxPriority<priority) maxPriority=priority;			 
		 }
		 /*
		 Integer index=0;
		 for (SecuredTaskBean item : list)
		 {
			 Integer newPriority=priorities.get(index)-(maxPriority-countItems)-1;
			 udfAccessor.SetUDFValueByAdmin(UDF_IDs.SectionPriority, item, String.valueOf(newPriority), false);
			 index++;
		 }
		 
		 */
		 // �������� �������� ���� ��������� �����������
		//��������� ���� =���������� ��������
		 
		 udfAccessor.SetUDFValueByAdmin(UDF_IDs.SectionPriority, arg0.getTask(), String.valueOf(maxPriority+1), false);
		}
		catch (Exception ex)
		{
			TaskCreator.CreateTaskForAdminAboutException("execute", this.getClass().getName(), "",  arg0.getTask().getNumber(), ex);
		}
		return arg0;
	}

}
