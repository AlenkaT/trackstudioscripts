package scripts.after_add_message;

import com.trackstudio.app.Slider;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.OperationTrigger;

import com.trackstudio.secured.SecuredMessageTriggerBean;
import com.trackstudio.secured.SecuredTaskBean;

import scripts.helpers.Filter_IDs;
import scripts.helpers.MessageGenerator;
import scripts.helpers.TaskCreator;
import scripts.helpers.TaskSearcher;
import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.ValueContainer;

public class CompleteSection implements OperationTrigger {

	@Override
	public SecuredMessageTriggerBean execute(SecuredMessageTriggerBean arg0) throws GranException {
		// TODO Auto-generated method stub
		
		// ������ ����� ����� SecuredMessageTriggerBean ����� �������� ������ ������ � �����, ������� ����� ���� ������������� � ��������, ��� �������
		// � SetNewSectionPriority (after trigger) ������ ��� � ���� SectionPriority
		// ��� Map<String, String> udfs=arg0.getUdfValues();
		/*udfs.forEach((k,v)->{
			try {
				MessageGenerator.CreateCommentMsg(arg0.getTask(), "key="+k+"  value="+v, false);
			} catch (GranException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		// �� ������������ ���� ������ ��� ������
		*/
		try
		{
			UDFValueAccessor accessor= new  UDFValueAccessor();
			//arg0.getTask().getHandlerUserId() = null !
			// ���� ���������� , ��� ��� ������������� � ������ ������
			ValueContainer vc=accessor.GetUDFValueByAdmin(UDF_IDs.ServiceField, arg0.getTask(), false);
			if (!vc.isExist() || ((String)vc.getValue()).isEmpty()) return null;
			
			accessor.SetUDFValueByAdmin(UDF_IDs.ServiceField, arg0.getTask(), null, false); //�������� ��������� ����, � ������� ��� �������� id ������������ , ������������ �������� ���������
			
			String lastHandlerUserId = (String)vc.getValue();
			
			Slider<SecuredTaskBean> slider=TaskSearcher.GetTaskListForUser(lastHandlerUserId, Filter_IDs.MySections,"1");// �������� ��� ���� �������� ������� �������
			vc=accessor.GetUDFValue("Integer", UDF_IDs.SectionPriority, arg0.getTask(),false);
			if (!vc.isExist() || vc.isEmpty()) return null;
			accessor.SetUDFValueByAdmin(UDF_IDs.SectionPriority, arg0.getTask(), null, false); // �������� ����������� ���������
			Integer priority = (Integer)vc.getValue(); //����������� �������������� ���������
			
			MessageGenerator.CreateCommentMsg(arg0.getTask(), "priority="+priority, false);
			
			for (SecuredTaskBean task:slider)
			{
				vc=accessor.GetUDFValueByAdmin(UDF_IDs.SectionPriority, task, false);
				if (!vc.isExist() || ((String)vc.getValue()).isEmpty()) continue;
				Integer currentPriority=Integer.parseInt((String)vc.getValue());
				if (currentPriority > priority)
				 {			
					accessor.SetUDFValueByAdmin(UDF_IDs.SectionPriority, task, String.valueOf(currentPriority-1) , false); // ��������� ����� �������� ���������� ������� ������
					MessageGenerator.CreateCommentMsg(task, "�������� ������������ ���������� ������ ���� ��������� ("+String.valueOf(currentPriority)+"->"+String.valueOf(currentPriority-1)+
							"), � ����� ����������� �������", false);
				 }
			}
			
		}
		
		catch (Exception ex)
		{
			TaskCreator.CreateTaskForAdminAboutException("execute", this.getClass().getName(), "",  arg0.getTask().getNumber(), ex);
		}
		return arg0;
	}

}
