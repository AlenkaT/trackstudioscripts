package scripts.after_add_message;


import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.OperationTrigger;
import com.trackstudio.kernel.manager.MessageManager;
import com.trackstudio.secured.SecuredMessageTriggerBean;
import com.trackstudio.secured.SecuredTaskBean;
import com.trackstudio.securedkernel.SecuredMessageAdapterManager;

import scripts.helpers.TaskCreator;
import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.ValueContainer;

public class MoveToAnotherProject implements OperationTrigger {

	@Override
	public SecuredMessageTriggerBean execute(SecuredMessageTriggerBean arg0) throws GranException {
		// TODO Auto-generated method stub
		
		// ������ ����� ����� SecuredMessageTriggerBean ����� �������� ������ ������ � �����, ������� ����� ���� ������������� � ��������, ��� �������
		// � SetNewSectionPriority (after trigger) ������ ��� � ���� SectionPriority
		// ��� Map<String, String> udfs=arg0.getUdfValues();
		/*udfs.forEach((k,v)->{
			try {
				MessageGenerator.CreateCommentMsg(arg0.getTask(), "key="+k+"  value="+v, false);
			} catch (GranException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		// �� ������������ ���� ������ ��� ������
		*/
		try
		{
			UDFValueAccessor accessor= new  UDFValueAccessor();
			ValueContainer vc = accessor.GetUDFValue(UDF_IDs.NewHeadProjectForLetter, arg0,false);
			if (vc.isExist() )
			{
				String str=(String)vc.getValue();
				if (!str.isEmpty())
				{
					SecuredTaskBean task=AdapterManager.getInstance().getSecuredTaskAdapterManager().findTaskByNumber(arg0.getSecure(), str.substring(str.lastIndexOf("#")+1));
					if (task!=null)
					{
						for (SecuredTaskBean t:task.getChildren())
						{
							if (t.getName().contentEquals(("!������")))
							{/*Parameters:
sc - ������ ������������
parentId - ID ������, ���� ���������
taskIds - ID ����������� �����
operation - ��� ���������, COPY ��� CUT
*/
								//String oldParent=arg0.getTask().getParent().getNumber();
								AdapterManager.getInstance().getSecuredTaskAdapterManager().pasteTasks(arg0.getSecure(), t.getId(), arg0.getTaskId(), "CUT");
								accessor.SetUDFValueByAdmin(UDF_IDs.NewHeadProjectForLetter, arg0.getTask(), null, false);
								//MessageManager.getMessage().updateDescription(arg0.getId(), "was moved from #"+oldParent);
								break;
							}
						}	
					}
					else
					{
						accessor.SetErrorUDFValue(arg0.getTask(), "������ "+str.substring(str.lastIndexOf("#"))+" ��� ����������� � ���� ������ �� ������" , false);
						//accessor.SetErrorUDFValue(arg0.getTask(), str.substring(str.lastIndexOf("#")) , false);
					}
				}
				
			}
		}
		
		catch (Exception ex)
		{
			TaskCreator.CreateTaskForAdminAboutException("execute", this.getClass().getName(), "",  arg0.getTask().getNumber(), ex);
		}
		return arg0;
	}

}
