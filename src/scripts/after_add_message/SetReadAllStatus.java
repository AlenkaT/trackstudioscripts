package scripts.after_add_message;

import java.io.File;
import java.io.FileWriter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.app.session.SessionContext;
import com.trackstudio.app.session.SessionManager;
import com.trackstudio.exception.GranException;

import com.trackstudio.exception.UserExceptionAfterTrigger;
import com.trackstudio.external.OperationTrigger;
import com.trackstudio.kernel.cache.UserRelatedManager;
import com.trackstudio.kernel.manager.MessageManager;

import com.trackstudio.kernel.manager.TaskManager;

import com.trackstudio.secured.SecuredMessageBean;
import com.trackstudio.secured.SecuredMessageTriggerBean;
import com.trackstudio.secured.SecuredTaskBean;

import com.trackstudio.secured.SecuredUserBean;

import scripts.helpers.TaskCreator;

import scripts.helpers.MessageGenerator;
import scripts.helpers.StringConstants;
import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.UserUDF_IDs;
import scripts.helpers.ValueContainer;

public class SetReadAllStatus implements OperationTrigger {
	private static final String _readMstatusId=		"402881816f228b86016fa86ec6721f61";
	private static final String _resendMstatusId=	"402881816f228b86016fa86dd9931f3c";
	private static final String _endStatusID="402881816ff5b7bf0170c8642b4040d5"; //������ ���������
	private static final String _readAllStatusID="402881816f228b86016fa86f8d8f1f86"; //������ ���������
	
	@Override
	public SecuredMessageTriggerBean execute(SecuredMessageTriggerBean arg0) throws GranException {
		   //�������� , ��� �� �� ���, ���� ���������� ���������, ��������� ������
		//�������� ��������� ������ ����� ����� TaskManager
		//���������� ������ � �������� ���� �� �������� ����������� (����, ������������� ������� �� ����������� � ����)
		SecuredTaskBean task=arg0.getTask();

		UDFValueAccessor udfAccessor= new UDFValueAccessor();
		ValueContainer vc = udfAccessor.GetUDFValue( UDF_IDs.NeedToSend, arg0, false);
		
		
		
		if (!vc.isExist())
		{
			TaskCreator.CreateTaskForAdmin("�������� ���� ���������� ��������� ��� ���������� �������� ������������� @"+arg0.getSubmitter().getLogin()+" �� ���� ��������, ����������� ���������� ������� ����������", arg0.getTask().getTaskNumber());
			return arg0;
		}
		
		udfAccessor.SetErrorUDFValue(arg0.getTask(),"", false);
		
		//Boolean readAll=true;
		String appendedDescription="";
			//String[] userNames = subscribedUserList.replaceAll("@", "").replaceAll(arg0.getSubmitter().getLogin(), "").split(";"); //���� �. ��������� //userNames[0] - ��������� ���������� �������� �����
		
		String subscribedUserValue=((String)vc.getValue()).replace(";",""); //������� �����������
		udfAccessor.AddErrorUDFValue(arg0.getTask(),subscribedUserValue, false);
		
		//udfAccessor.SetErrorUDFValue(arg0.getTask(), "subscribedUserValue:"+subscribedUserValue, false);
		if (subscribedUserValue.length()>1)
		{

			String[] userNames = subscribedUserValue.split("@"); // ������ ������ , ��������, ��� ������ ������� - ������ ������
	

			appendedDescription= System.lineSeparator()+"������ ��������� ������, ���� ������������ @"+ userNames[userNames.length-1]+" �� �������� ��� ������";
				String oldDescription=arg0.getDescription();
				MessageManager.getMessage().updateDescription(arg0.getId(), oldDescription+"<br>"+appendedDescription);
				
				if (task.getStatusId().contentEquals(_endStatusID) || task.getStatusId().contentEquals(_readAllStatusID))
		    	{
					//������ ��� �������� ��������! - ����������
		    		ArrayList<SecuredMessageBean> msgs= arg0.getTask().getMessages();
		    		for(SecuredMessageBean msg:msgs)
		    		{
		    			if (msg.getMstatusId().contentEquals(_readMstatusId))
		    			{
		    				continue;
		    			}
		    			TaskManager.getTask().updateTaskStatus(task.getId(), "402881816f228b86016fa86fa7201f87"); //������������� ������ ��������������
		    		}
		    		
		    		TaskManager.getTask().updateTaskStatus(task.getId(), "402881816f228b86016fa86b9ca41f15"); //������������� ������ ��������
		    	}
				
				
				
				//16.03.2021 - �������� ���� ����������� ������ ������ "���������� ���������"
			    
				Calendar calendar = Calendar.getInstance();
				SimpleDateFormat formater = new SimpleDateFormat("dd.MM.yyyy");
				Boolean lastLog=false;
				String message="";
				
			    for (String userName : userNames)
			    {
			    	udfAccessor.SetErrorUDFValue(arg0.getTask(), "userName:"+userName, false);
			    	if (userName.isEmpty()) continue;
			    	SecuredUserBean userBean=AdapterManager.getInstance().getSecuredFindAdapterManager().searchUserByQuickGo(
			    			SessionManager.getInstance().getSessionContext(SessionManager.getInstance().create(UserRelatedManager.getInstance().find(StringConstants.AdminId))), userName.substring(1) );
			    	
			    	SessionContext sc= SessionManager.getInstance().getSessionContext(SessionManager.getInstance().create(userBean.getUser()));
						
					ValueContainer vcUser=udfAccessor.GetUserUDFValue(UserUDF_IDs.NetWorkUserName, sc); // ������� ��� ������������
					Calendar lastDate=Calendar.getInstance();
					
					//udfAccessor.SetErrorUDFValue(arg0.getTask(), "lastDate:"+lastDate, false);
					if (vcUser.isExist() )			
					{
						
						File file= new File("D:\\NetworkDrives\\Obmen\\TS\\Requests\\NewEvents\\"+vcUser.getValue()+".txt");
						
						File file1= new File("D:\\NetworkDrives\\Obmen\\TS\\EVT\\Logs\\NewTrackStudioEvents\\"+vcUser.getValue()+".txt");
						
						if (!file.exists() && !file1.exists())
							{
							
								lastDate.setTime(new Date(0));
								udfAccessor.AddErrorUDFValue(arg0.getTask(), "����� �� �������", false);
							}
						else
						{
							
							if (file.exists() )
							{
								lastDate.setTime(new Date(file.lastModified()));	
								udfAccessor.AddErrorUDFValue(arg0.getTask(), "file.exists", false);
							}
							if (file1.exists() )
							{
								lastDate.setTime(new Date(file1.lastModified()));	
								udfAccessor.AddErrorUDFValue(arg0.getTask(), "file1.exists", false);
							}
							
						}
					}	
			    	
			    	
			    	//if (calendar.get(Calendar.DAY_OF_YEAR)!=userBean.getLastLogonDate().get(Calendar.DAY_OF_YEAR) || calendar.get(Calendar.YEAR)!=userBean.getLastLogonDate().get(Calendar.YEAR))
					if (calendar.get(Calendar.DAY_OF_YEAR)!=lastDate.get(Calendar.DAY_OF_YEAR) || calendar.get(Calendar.YEAR)!=lastDate.get(Calendar.YEAR))
					{
			    		//AdapterManager.getInstance().getSecuredMessageAdapterManager().getMessageUserList(sc, ownerId)
			    		lastLog=true;
			    		message=message+ 
			    				"��������������!: ������������ "+userBean.getName()+" ������� �� ������� ����������� TrackStudio ��� ������� ����������� � ���� �������� ��������� (��������� ���� ������ ���� ����������� - "
			    				+formater.format(//userBean.getLastLogonDate().getTime())+")"+"<br>";
			    						lastDate.getTime())+", ��������� ���� ����������� � TrackStudio -"+formater.format(userBean.getLastLogonDate().getTime())+")"+"<br>";
			    	}
				}
				

			    if (lastLog)
			    {
			    	MessageGenerator.CreateCommentMsg(arg0.getTask(), message, false);
			    	throw new UserExceptionAfterTrigger(message);
			    	//udfAccessor.SetErrorUDFValue(arg0.getTask(),message , false);
			    	
			    }
			    
				return arg0;	
		}
		
		if (!task.getStatusId().contentEquals(_endStatusID) )
    	{
			TaskManager.getTask().updateTaskStatus(task.getId(), _readAllStatusID); //������������� ������ ���������
			//��� ����������� ���!
			//MessageGenerator.CreateCommentMsg(arg0.getTask(), "������ ��� �������� ������ - \"���������\"", true); //�� �������� ����� ��������� ������� (((
    	}
	    
		return arg0;
	    	
	}

}
