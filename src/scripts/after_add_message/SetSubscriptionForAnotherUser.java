package scripts.after_add_message;


import com.trackstudio.exception.GranException;
import com.trackstudio.external.OperationTrigger;
import com.trackstudio.kernel.manager.SafeString;
import com.trackstudio.kernel.manager.TaskManager;
import com.trackstudio.secured.SecuredMessageTriggerBean;
import com.trackstudio.secured.SecuredTaskBean;
import scripts.helpers.TaskCreator;
import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.ValueContainer;

public class SetSubscriptionForAnotherUser implements OperationTrigger {
	
	@Override
	public SecuredMessageTriggerBean execute(SecuredMessageTriggerBean arg0) throws GranException {
		// TODO Auto-generated method stub
		
		//
		try
		{
			UDFValueAccessor accessor= new  UDFValueAccessor();
			//arg0.getTask().getHandlerUserId() = null !
			
			ValueContainer vc=accessor.GetUDFValue(UDF_IDs.SubscribedUsers, arg0,false);
			if (!vc.isExist() || ((String)vc.getValue()).isEmpty()) return null;
			ValueContainer vc1= accessor.GetUDFValueByAdmin(UDF_IDs.SubscribedUsersServ, arg0.getTask(), false);
			if (!vc1.isExist() ) return null;
			accessor.SetUDFValueByAdmin(UDF_IDs.SubscribedUsersServ, arg0.getTask(), (String)vc.getValue(), false); //���������� �������� � ��������� ����
			accessor.SetUDFValueByAdmin(UDF_IDs.SubscribedUsers, arg0.getTask(), null, false); //�������� ����������������
		/*
			String lastHandlerUserId = (String)vc.getValue();
			
			//���������� ��������� �������������� ������
			SecuredTaskBean task = arg0.getTask();
			//�� ��������
			TaskManager.getTask().updateTask(task.getId(), SafeString.createSafeString(task.getShortname()), SafeString.createSafeString(task.getName()), SafeString.createSafeString(task.getDescription()), 
					task.getBudget(), task.getDeadline(), task.getPriorityId(), task.getParentId(), lastHandlerUserId , null, 
					task.getSubmitdate(), task.getUpdatedate());
					*/
		}
		
		catch (Exception ex)
		{
			TaskCreator.CreateTaskForAdminAboutException("execute", this.getClass().getName(), "",  arg0.getTask().getNumber(), ex);
		}
		return arg0;
	}

}
