package scripts.after_add_message;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import com.trackstudio.app.Slider;
import com.trackstudio.app.adapter.AdapterManager;
import com.trackstudio.app.adapter.email.change.NewMessageChange;
import com.trackstudio.app.session.SessionContext;
import com.trackstudio.app.session.SessionManager;
import com.trackstudio.exception.GranException;
import com.trackstudio.external.OperationTrigger;
import com.trackstudio.kernel.cache.UserRelatedManager;
import com.trackstudio.kernel.manager.MessageManager;
import com.trackstudio.kernel.manager.SafeString;
import com.trackstudio.kernel.manager.TaskManager;
import com.trackstudio.kernel.manager.UdfManager;
import com.trackstudio.secured.SecuredMessageBean;
import com.trackstudio.secured.SecuredMessageTriggerBean;
import com.trackstudio.secured.SecuredTaskBean;

import scripts.helpers.TaskCreator;
import scripts.helpers.TaskSearcher;
import scripts.helpers.DescriptionAccessor;
import scripts.helpers.Filter_IDs;
import scripts.helpers.MessageGenerator;
import scripts.helpers.UDFValueAccessor;
import scripts.helpers.UDF_IDs;
import scripts.helpers.ValueContainer;

public class SetNewSectionPriority implements OperationTrigger {
	
	@Override
	public SecuredMessageTriggerBean execute(SecuredMessageTriggerBean arg0) throws GranException {
		UDFValueAccessor accessor= new  UDFValueAccessor();
		try
		{
			ValueContainer vc=accessor.GetUDFValue(UDF_IDs.SectionPriority, arg0, false);
			if (!vc.isEmpty() || ((String)vc.getValue()).isEmpty()) 
			{
				//�� ������� �������� ��������
				return null;
			}
			Integer newPriority = Integer.parseInt((String)vc.getValue()); //�������� ����������
			
			Integer oldPriority = Integer.parseInt((String) accessor.GetUDFValueByAdmin(UDF_IDs.ServiceField, arg0.getTask(), false).getValue()); //���� �.�. ��������������� � �������������, ����� ��� �������� ���������� �������� ������� ���������� �������������� ���������
			// ������ �������� ���������
			DescriptionAccessor.SetMessageDescriptionByAdapterManager(arg0, "�������� ���������� ���� �������� <br>   ����: "+oldPriority+"<br>   �����: "+ newPriority);
			//�������� ��� ����
			accessor.SetUDFValueByAdmin(UDF_IDs.ServiceField, arg0.getTask(), null, false); // �������� ��������� ���� (������ �������� ����������)
			accessor.SetUDFValueByAdmin(UDF_IDs.CoordinatorsList, arg0.getTask(), null, false); // �������� ���� ������������ ������� �� ���������
			accessor.SetUDFValueByAdmin(UDF_IDs.RequestedPriority, arg0.getTask(), null, false); // �������� ��������� ���������
		
			Slider<SecuredTaskBean> slider=TaskSearcher.GetTaskListForUser(arg0.getTask().getHandlerUserId(), Filter_IDs.MySections,"1");// �������� ��� ���� �������� ������� �������
			//SecuredMessageBean msgBean= AdapterManager.getInstance().getSecuredFindAdapterManager().findMessageById(arg0.getSecure(), arg0.getId());
		
		 String  newvalue;
		 for (SecuredTaskBean task:slider)
		 {
			 if (task.getNumber()==arg0.getTask().getNumber()) continue;
			 vc=accessor.GetUDFValueByAdmin(UDF_IDs.SectionPriority, task, false);
			// TaskCreator.CreateTaskForAdmin(vc.toString(), arg0.getTask().getNumber());
			 if (!vc.isExist() || ((String)vc.getValue()).isEmpty()) continue;
			 Integer currentPriority=Integer.parseInt((String)vc.getValue());
			 if (currentPriority >= newPriority && currentPriority < oldPriority)
			 {
				 newvalue=String.valueOf(currentPriority+1);
				 accessor.SetUDFValueByAdmin(UDF_IDs.SectionPriority, task, newvalue , false); // ��������� ����� �������� ���������� ����������� �������
				 
				MessageGenerator.CreateCommentMsg(task, "�������� ������������ ���������� ������ ���� ������� ("+String.valueOf(currentPriority)+"->"+String.valueOf(currentPriority+1)+
						"), � ����� � ������������� ����� �������� ���������� ��� ������ #"+arg0.getTask().getNumber()+" ������������� @"+arg0.getSubmitter().getLogin(), true);
			 }
		 }
		}
		catch (Exception ex)
		{
			TaskCreator.CreateTaskForAdminAboutException("execute", this.getClass().getName(), "",  arg0.getTask().getNumber(), ex);
		}
		return null;
	    	
	}

}
