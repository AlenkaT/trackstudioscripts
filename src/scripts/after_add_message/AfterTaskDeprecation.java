package scripts.after_add_message;


import com.trackstudio.exception.GranException;
import com.trackstudio.external.OperationTrigger;
import com.trackstudio.secured.SecuredMessageTriggerBean;

import scripts.helpers.TaskEditor;

public class AfterTaskDeprecation implements OperationTrigger {

	@Override
	public SecuredMessageTriggerBean execute(SecuredMessageTriggerBean arg0) throws GranException {
		// TODO Auto-generated method stub
		
		TaskEditor.ChangeTaskNumbers(arg0.getTask(),-1, false);
		return null;
	}

}
